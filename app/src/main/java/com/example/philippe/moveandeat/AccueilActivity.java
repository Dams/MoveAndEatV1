package com.example.philippe.moveandeat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Fragments.DashboardFragment;
import com.example.philippe.moveandeat.Fragments.ExercicesFragment;
import com.example.philippe.moveandeat.Fragments.ProfilFragment;
import com.example.philippe.moveandeat.Fragments.RecettesFragments;

public class AccueilActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TextView TXT_Nav_Name, TXT_Nav_Objectif;
    private UserDataService userDataService;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accueil);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Changement du nom et du prénom dans le menu
        View header = navigationView.getHeaderView(0);
        TXT_Nav_Name = (TextView) header.findViewById(R.id.TXT_Nav_Name);
        TXT_Nav_Objectif = (TextView) header.findViewById(R.id.TXT_Nav_Objectif);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }else{
                    userDataService = new UserDataService(AccueilActivity.this);
                    userDataService.open();
                    TXT_Nav_Name.setText(userDataService.getUser().getPrenomU());
                    switch (userDataService.getUser().getObjectif()){
                        case 1:
                            TXT_Nav_Objectif.setText("Objectif : Prendre du muscle");
                            break;
                        case 2:
                            TXT_Nav_Objectif.setText("Objectif : Perdre du gras");
                            break;
                        case 3:
                            TXT_Nav_Objectif.setText("Objectif : Garder son poids");
                            break;
                        default:
                            break;
                    }
                    userDataService.close();
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });


        /* Ouverture du Fragment Profil des le départ de l'activité */
        Fragment fragment = null;
        Class fragmentClass = DashboardFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        setTitle(navigationView.getMenu().getItem(0).getTitle());
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        /*
            Initialisation de deux éléments, un objet Fragment et un objet Class qui sont
            tous deux nuls pour le moment.
         */
        Fragment fragment = null;
        Class fragmentClass = null;

        int id = item.getItemId();
        if (id == R.id.nav_profil) {
            /*
                Au clique sur un élément du menu (pour ce cas la l'utilisateur clique sur "profil")
                l'objet fragmentClass prendra la valeur ProfilFragment.class qui est la classe correspondant
                au fragment du profil de l'utilisateur.
             */
            fragmentClass = ProfilFragment.class;
        } else if (id == R.id.nav_accueil) {
            fragmentClass = DashboardFragment.class;
        } else if (id == R.id.nav_exercice) {
            fragmentClass = ExercicesFragment.class;
        } else if (id == R.id.nav_recette) {
            fragmentClass = RecettesFragments.class;
        } else if (id == R.id.nav_quitter) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AccueilActivity.this);
            builder.setTitle("Attention");
            builder.setMessage("Voulez-vous vraiment quitter l'application ?");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

        /*
            Si l'objet fragmentClass n'est pas null c'est qu'il faut ouvrir le fragment sur lequel à
            cliqué l'utilisateur.
         */
        if (fragmentClass != null) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*
                On utilise le FragmentManager pour gérer des fragments.
                Dans notre cas, le fragmant manager va remplacer l'ancien fragment pas celui qui est dans
                la variable fragment (ou le créer si il n'y a aucun fragment avant).
                Les fragments sont mis dans le FrameLayout (accueil.xml)
             */
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

            setTitle(item.getTitle());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
