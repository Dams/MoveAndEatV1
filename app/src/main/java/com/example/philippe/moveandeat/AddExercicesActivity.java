package com.example.philippe.moveandeat;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Object.FamilleExercices;

import java.util.ArrayList;
import java.util.List;

public class AddExercicesActivity extends AppCompatActivity {

    private Toolbar Toolbar_AddExercice;
    private EditText EditTXT_AddExercice;
    private Spinner SPIN_AddExercice;
    private Button BTN_AddExercice;
    private UserDataService userDataService;
    private List<FamilleExercices> list_famille;
    private List<String> list_name_famille = new ArrayList<String>();
    private int id_famille_selected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_exercices);

        Toolbar_AddExercice = (Toolbar) findViewById(R.id.Toolbar_AddExercice);
        EditTXT_AddExercice = (EditText) findViewById(R.id.EditTXT_AddExercice);
        SPIN_AddExercice = (Spinner) findViewById(R.id.SPIN_AddExercice);
        BTN_AddExercice = (Button) findViewById(R.id.BTN_AddExercice);
        Toolbar_AddExercice.setTitle("Ajouter un exercice");
        Toolbar_AddExercice.setTitleTextColor(Color.rgb(255, 255, 255));
        setSupportActionBar(Toolbar_AddExercice);

        //Préparation du Spinner
        userDataService = new UserDataService(AddExercicesActivity.this);
        userDataService.open();
        list_famille = userDataService.getFamilleExercices();
        userDataService.close();

        for (int i = 0; i < list_famille.size(); i++){
            list_name_famille.add(list_famille.get(i).getLibelle());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list_name_famille);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPIN_AddExercice.setAdapter(dataAdapter);

        SPIN_AddExercice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                id_famille_selected = list_famille.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                id_famille_selected = 1;
            }
        });

        BTN_AddExercice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!EditTXT_AddExercice.getText().toString().equals("")){
                    //Création de l'exercice
                    userDataService.open();
                    userDataService.createExercice(EditTXT_AddExercice.getText().toString(), "", "", id_famille_selected);
                    userDataService.close();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Merci d'entrer le nom de l'exercice.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
