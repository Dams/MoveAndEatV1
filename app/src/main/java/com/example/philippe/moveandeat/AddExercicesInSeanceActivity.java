package com.example.philippe.moveandeat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Object.Exercices;
import com.example.philippe.moveandeat.Object.FamilleExercices;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddExercicesInSeanceActivity extends AppCompatActivity {

    private Toolbar Toolbar_AddExercicesInSeance;
    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<Exercices>> listDataChild;
    private UserDataService userDataService;
    private int id_seance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_exercices_in_seance);

        Intent intent = getIntent();
        id_seance = intent.getIntExtra("id_seance", 0);

        Toolbar_AddExercicesInSeance = (Toolbar) findViewById(R.id.Toolbar_AddExercicesInSeance);
        Toolbar_AddExercicesInSeance.setTitle("Ajouter un exercice à la séance");
        Toolbar_AddExercicesInSeance.setTitleTextColor(Color.rgb(255, 255, 255));
        setSupportActionBar(Toolbar_AddExercicesInSeance);

        userDataService = new UserDataService(AddExercicesInSeanceActivity.this);
        userDataService.open();
        final List<Exercices> list_exos_theorique = userDataService.getExercices();
        userDataService.close();

        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        prepareListData();
        listAdapter = new ExpandableListAdapter(AddExercicesInSeanceActivity.this, listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                //Ajout de l'exercice sélectionner dans la séance
                Exercices exercices = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                userDataService = new UserDataService(AddExercicesInSeanceActivity.this);
                userDataService.open();
                userDataService.addExerciceToSeance(exercices.getId(), id_seance, 15, 3, 30);
                userDataService.close();
                finish();
                return false;
            }
        });





    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Exercices>>();

        //Récupération des familles d'exercices
        UserDataService userDataService = new UserDataService(getApplicationContext());
        userDataService.open();
        List<FamilleExercices> list = userDataService.getFamilleExercices();
        userDataService.close();

        //Ajout des noms des familles d'exercices dans la header list
        for (int i =0; i < list.size(); i++){
            listDataHeader.add(list.get(i).getLibelle());
        }

        //Récupération des exercices
        userDataService.open();
        List<Exercices> list_exos = userDataService.getExercices();
        userDataService.close();


        List<Exercices> list_exo_abdos = new ArrayList<Exercices>();
        List<Exercices> list_exo_avant_bras = new ArrayList<Exercices>();
        List<Exercices> list_exo_biceps = new ArrayList<Exercices>();
        List<Exercices> list_exo_dos = new ArrayList<Exercices>();
        List<Exercices> list_exo_epaules = new ArrayList<Exercices>();
        List<Exercices> list_exo_fessiers = new ArrayList<Exercices>();
        List<Exercices> list_exo_ischios_jambiers = new ArrayList<Exercices>();
        List<Exercices> list_exo_lombaires = new ArrayList<Exercices>();
        List<Exercices> list_exo_mollets = new ArrayList<Exercices>();
        List<Exercices> list_exo_pectoraux = new ArrayList<Exercices>();
        List<Exercices> list_exo_quadriceps = new ArrayList<Exercices>();
        List<Exercices> list_exo_trapezes = new ArrayList<Exercices>();
        List<Exercices> list_exo_triceps = new ArrayList<Exercices>();

        //Parcours de tous les exercices
        for (int i = 0; i < list_exos.size(); i++){
            switch (list_exos.get(i).getId_famille()){
                case 1:
                    list_exo_abdos.add(list_exos.get(i));
                    break;
                case 2:
                    list_exo_avant_bras.add(list_exos.get(i));
                    break;
                case 3:
                    list_exo_biceps.add(list_exos.get(i));
                    break;
                case 4:
                    list_exo_dos.add(list_exos.get(i));
                    break;
                case 5:
                    list_exo_epaules.add(list_exos.get(i));
                    break;
                case 6:
                    list_exo_fessiers.add(list_exos.get(i));
                    break;
                case 7:
                    list_exo_ischios_jambiers.add(list_exos.get(i));
                    break;
                case 8:
                    list_exo_lombaires.add(list_exos.get(i));
                    break;
                case 9:
                    list_exo_mollets.add(list_exos.get(i));
                    break;
                case 10:
                    list_exo_pectoraux.add(list_exos.get(i));
                    break;
                case 11:
                    list_exo_quadriceps.add(list_exos.get(i));
                    break;
                case 12:
                    list_exo_trapezes.add(list_exos.get(i));
                    break;
                case 13:
                    list_exo_triceps.add(list_exos.get(i));
                    break;
                default:
                    break;
            }
        }

        listDataChild.put(listDataHeader.get(0), list_exo_abdos); // Header, Child data
        listDataChild.put(listDataHeader.get(1), list_exo_avant_bras);
        listDataChild.put(listDataHeader.get(2), list_exo_biceps);
        listDataChild.put(listDataHeader.get(3), list_exo_dos);
        listDataChild.put(listDataHeader.get(4), list_exo_epaules);
        listDataChild.put(listDataHeader.get(5), list_exo_fessiers);
        listDataChild.put(listDataHeader.get(6), list_exo_ischios_jambiers);
        listDataChild.put(listDataHeader.get(7), list_exo_lombaires);
        listDataChild.put(listDataHeader.get(8), list_exo_mollets);
        listDataChild.put(listDataHeader.get(9), list_exo_pectoraux);
        listDataChild.put(listDataHeader.get(10), list_exo_quadriceps);
        listDataChild.put(listDataHeader.get(11), list_exo_trapezes);
        listDataChild.put(listDataHeader.get(12), list_exo_triceps);
    }

    private class ExpandableListAdapter extends BaseExpandableListAdapter {

        private Context _context;
        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, List<Exercices>> _listDataChild;

        public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                     HashMap<String, List<Exercices>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        /*final String childText = (String) getChild(groupPosition, childPosition);*/
            Exercices child = (Exercices) getChild(groupPosition, childPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item, null);
            }

            TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
            ImageView IMG_Exercice = (ImageView) convertView.findViewById(R.id.IMG_Exercice);

            txtListChild.setText(child.getLibelle());
            if(!child.getPath_image().equals("") && !child.getPath_image().equals(null)){
                String uri = "@drawable/" + child.getPath_image();
                int imageResource = _context.getResources().getIdentifier(uri, null, _context.getPackageName());
                Drawable res = _context.getResources().getDrawable(imageResource);
                IMG_Exercice.setImageDrawable(res);
            }else{
                Picasso.with(getApplicationContext()).load(R.drawable.crunch).into(IMG_Exercice);
            }

            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this._listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {

            String headerTitle = (String) getGroup(groupPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }

            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);
            lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
