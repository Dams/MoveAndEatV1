package com.example.philippe.moveandeat;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Object.FamilleExercices;
import com.example.philippe.moveandeat.Object.FamilleRecettes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Damian on 25/06/2017.
 */

public class AddRecetteActivity extends AppCompatActivity {

    private Toolbar Toolbar_AddRecette;
    private EditText EditTXT_AddRecette;
    private EditText EditTXT_Ingredients;
    private EditText EditTXT_Preparation;
    private Spinner SPIN_AddRecette;
    private Button BTN_AddRecette;
    private UserDataService userDataService;
    private List<FamilleRecettes> list_famille;
    private List<String> list_name_famille = new ArrayList<String>();
    private int id_famille_selected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recette);
        Toolbar_AddRecette = (Toolbar) findViewById(R.id.Toolbar_AddRecette);
        EditTXT_AddRecette = (EditText) findViewById(R.id.EditTXT_AddRecette);
        EditTXT_Ingredients = (EditText) findViewById(R.id.EditTXT_Ingredients);
        EditTXT_Preparation = (EditText) findViewById(R.id.EditTXT_Preparation);
        SPIN_AddRecette = (Spinner) findViewById(R.id.SPIN_AddRecette);
        BTN_AddRecette = (Button) findViewById(R.id.BTN_AddRecette);
        Toolbar_AddRecette.setTitle("Ajouter une recette");
        Toolbar_AddRecette.setTitleTextColor(Color.rgb(255, 255, 255));
        setSupportActionBar(Toolbar_AddRecette);
        //Préparation du Spinner
        userDataService = new UserDataService(AddRecetteActivity.this);
        userDataService.open();
        list_famille = userDataService.getFamilleRecettes();
        userDataService.close();

        for (int i = 0; i < list_famille.size(); i++){
            list_name_famille.add(list_famille.get(i).getLibelle());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list_name_famille);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPIN_AddRecette.setAdapter(dataAdapter);

        SPIN_AddRecette.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                id_famille_selected = list_famille.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                id_famille_selected = 1;
            }
        });

        BTN_AddRecette.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!EditTXT_AddRecette.getText().toString().equals("") && !EditTXT_Ingredients.getText().toString().equals("") && !EditTXT_Preparation.getText().toString().equals("")){
                    //Création de l'exercice
                    userDataService.open();
                    userDataService.createRecette(EditTXT_AddRecette.getText().toString(), "", EditTXT_Ingredients.getText().toString(), EditTXT_Preparation.getText().toString(),id_famille_selected);
                    userDataService.close();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Merci de remplir les champs vides.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
