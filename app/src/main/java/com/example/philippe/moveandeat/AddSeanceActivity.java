package com.example.philippe.moveandeat;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;

public class AddSeanceActivity extends AppCompatActivity {

    private Toolbar Toolbar_AddSeance;
    private EditText EditTXT_AddSeance_Name, EditTXT_AddSeance_Description;
    private Button BTN_AddSeance;
    private UserDataService userDataService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_seance);

        Toolbar_AddSeance = (Toolbar) findViewById(R.id.Toolbar_AddSeance);
        EditTXT_AddSeance_Name = (EditText) findViewById(R.id.EditTXT_AddSeance_Name);
        EditTXT_AddSeance_Description = (EditText) findViewById(R.id.EditTXT_AddSeance_Description);
        BTN_AddSeance = (Button) findViewById(R.id.BTN_AddSeance);
        Toolbar_AddSeance.setTitle("Ajouter une séance");
        Toolbar_AddSeance.setTitleTextColor(Color.rgb(255, 255, 255));
        setSupportActionBar(Toolbar_AddSeance);

        BTN_AddSeance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!EditTXT_AddSeance_Description.getText().toString().equals("")
                        && !EditTXT_AddSeance_Name.getText().toString().equals("")){
                    userDataService = new UserDataService(AddSeanceActivity.this);
                    userDataService.open();
                    userDataService.createSeance(EditTXT_AddSeance_Name.getText().toString(),
                            EditTXT_AddSeance_Description.getText().toString(),
                            0);
                    userDataService.close();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Merci de remplir tous les champs pour créer votre séance.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
