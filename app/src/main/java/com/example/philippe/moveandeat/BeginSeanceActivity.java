package com.example.philippe.moveandeat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Object.ExericeSeance;
import com.example.philippe.moveandeat.Object.User;

import java.util.ArrayList;
import java.util.List;

public class BeginSeanceActivity extends AppCompatActivity {

    private int id_seance;
    private String name_seance;
    private Toolbar Toolbar_BeginSeance;
    private TextView TXT_BeginSeance_NameFirstExo, TXT_BeginSeance_Nb_Series, TXT_BeginSeance_TempsRepos;
    private Button BTN_BeginSeance;
    private List<ExericeSeance> list = new ArrayList<ExericeSeance>();
    private UserDataService userDataService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_begin_seance);

        final Intent intent = getIntent();
        id_seance = intent.getIntExtra("id", 0);
        name_seance = intent.getStringExtra("name");

        Toolbar_BeginSeance = (Toolbar) findViewById(R.id.Toolbar_BeginSeance);
        TXT_BeginSeance_NameFirstExo = (TextView) findViewById(R.id.TXT_BeginSeance_NameFirstExo);
        TXT_BeginSeance_Nb_Series = (TextView) findViewById(R.id.TXT_BeginSeance_Nb_Series);
        TXT_BeginSeance_TempsRepos = (TextView) findViewById(R.id.TXT_BeginSeance_TempsRepos);
        BTN_BeginSeance = (Button) findViewById(R.id.BTN_BeginSeance);
        Toolbar_BeginSeance.setTitle(name_seance);
        Toolbar_BeginSeance.setTitleTextColor(Color.rgb(255, 255, 255));
        setSupportActionBar(Toolbar_BeginSeance);

        //Récupération des exercices de la séance
        userDataService = new UserDataService(BeginSeanceActivity.this);
        userDataService.open();
        list = userDataService.getExercicesInSeance(id_seance);
        userDataService.close();

        TXT_BeginSeance_NameFirstExo.setText(list.get(0).getLibelle());
        TXT_BeginSeance_Nb_Series.setText(list.get(0).getNb_repetition() + "\n répétitions");
        TXT_BeginSeance_TempsRepos.setText(list.get(0).getTemps_repos() + "''\n de repos");

        BTN_BeginSeance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BTN_BeginSeance.setText("3");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        BTN_BeginSeance.setText("2");
                    }
                }, 1000);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        BTN_BeginSeance.setText("1");
                        Intent intent1 = new Intent(BeginSeanceActivity.this, ProgressSeanceActivity.class);
                        intent1.putExtra("id", id_seance);
                        startActivity(intent1);
                        finish();
                    }
                }, 2000);
            }
        });
        
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
