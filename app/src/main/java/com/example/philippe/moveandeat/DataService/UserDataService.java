package com.example.philippe.moveandeat.DataService;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.philippe.moveandeat.Object.Exercices;
import com.example.philippe.moveandeat.Object.ExericeSeance;
import com.example.philippe.moveandeat.Object.FamilleExercices;
import com.example.philippe.moveandeat.Object.FamilleRecettes;
import com.example.philippe.moveandeat.Object.Recettes;
import com.example.philippe.moveandeat.Object.Seances;
import com.example.philippe.moveandeat.Object.User;

import java.util.ArrayList;
import java.util.List;

public class UserDataService {

    /**
     * Description générale de la classe :
     *
     *      Cette classe comprend toute les requetes de la base de données.
     *      Elle permet donc de faire des SELECT, INSERT, UPDATE, DELETE sur les éléments présents en base de données,
     *      c'est à dire :
     *          - L'utilisateur
     *          - Les exercices
     *          - Les familles d'exercices
     *          - Les séances
     *          - La table seance_exercice qui est la table pivot entre séance et exerice
     */


    /**
     * Attributs de classe
     */
    private SQLiteDatabase database;
    private com.example.philippe.moveandeat.Utils.SQLiteOpenHelper sqLiteOpenHelper;

    private String[] allColumnsUser = {
            sqLiteOpenHelper.COLUMN_USER_ID,
            sqLiteOpenHelper.COLUMN_USER_FIRST_NAME,
            sqLiteOpenHelper.COLUMN_USER_SEXE,
            sqLiteOpenHelper.COLUMN_USER_POIDS,
            sqLiteOpenHelper.COLUMN_USER_TAILLE,
            sqLiteOpenHelper.COLUMN_USER_OBJECTIF,
            sqLiteOpenHelper.COLUMN_USER_CREATED_AT,
            sqLiteOpenHelper.COLUMN_USER_XP,
            sqLiteOpenHelper.COLUMN_USER_LEVEL};

    private String[] allColumnsFamilleExercices = {
            sqLiteOpenHelper.COLUMN_FAMILLE_EXERCICES_ID,
            sqLiteOpenHelper.COLUMN_FAMILLE_EXERCICES_LIBELLE};

    private String[] allColumnsExercices = {
            sqLiteOpenHelper.COLUMN_EXERCICES_ID,
            sqLiteOpenHelper.COLUMN_EXERCICES_LIBELLE,
            sqLiteOpenHelper.COLUMN_EXERCICES_IMAGE,
            sqLiteOpenHelper.COLUMN_EXERCICES_DESCRIPTION,
            sqLiteOpenHelper.COLUMN_EXERCICES_ID_FAMILLE};

    private String[] allColumnsSeances = {
            sqLiteOpenHelper.COLUMN_SEANCES_ID,
            sqLiteOpenHelper.COLUMN_SEANCES_LIBELLE,
            sqLiteOpenHelper.COLUMN_SEANCES_DESCRIPTION,
            sqLiteOpenHelper.COLUMN_SEANCES_IS_PREDEF};

    private String[] allColumnsSeancesExercices = {
            sqLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID,
            sqLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID_SEANCE,
            sqLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID_EXERCICE,
            sqLiteOpenHelper.COLUMN_SEANCES_EXERCICES_NB_REPETITIONS,
            sqLiteOpenHelper.COLUMN_SEANCES_EXERCICES_NB_SERIES,
            sqLiteOpenHelper.COLUMN_SEANCES_EXERCICES_TEMPS_REPOS};

    private String[] allColumnsRecettes = {
            sqLiteOpenHelper.COLUMN_RECETTES_ID,
            sqLiteOpenHelper.COLUMN_RECETTES_LIBELLE,
            sqLiteOpenHelper.COLUMN_RECETTES_IMAGE,
            sqLiteOpenHelper.COLUMN_RECETTES_INGREDIENTS,
            sqLiteOpenHelper.COLUMN_RECETTES_PREPARATION,
            sqLiteOpenHelper.COLUMN_RECETTES_ID_FAMILLE};

    private String[] getAllColumnsFamilleRecettes = {
            sqLiteOpenHelper.COLUMN_FAMILLE_RECETTES_ID,
            sqLiteOpenHelper.COLUMN_FAMILLE_RECETTES_LIBELLE};

    /**
     * Constructeur de la classe UserDataService
     * @param context
     */
    public UserDataService(Context context) {
        sqLiteOpenHelper = new com.example.philippe.moveandeat.Utils.SQLiteOpenHelper(context);
    }

    /**
     * Permet d'ouvrir la connexion à la base de données
     * @throws SQLException Dans le cas où il y aurait une exception
     */
    public void open() throws SQLException {
        database = sqLiteOpenHelper.getWritableDatabase();
    }

    /**
     * Permet de fermer la connexion de la base de données
     */
    public void close() {
        sqLiteOpenHelper.close();
    }


    /**
     * Fonction permettant la création d'un utilisateur dans la base de données
     * @param first_name
     * @param poids
     * @param taille
     */
    public void createUser(String first_name, String sexe, String poids, String taille, String objectif, String xp, int level){
        ContentValues values = new ContentValues();

        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_FIRST_NAME, first_name);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_SEXE, sexe);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_POIDS, poids);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_TAILLE, taille);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_OBJECTIF, objectif);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_XP, xp);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_LEVEL, level);

        database.insert(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_USER, null, values);
    }

    /**
     * Fonction permettant de récupérer les informations de l'utilisateur
     * @return
     */
    public User getUser() {
        User user = new User();
        Cursor cursor = database.query(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_USER,
                allColumnsUser, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            user = cursorToUser(cursor);
            cursor.moveToNext();
        }
        cursor.close();
        return user;
    }

    /**
     * Cursor permettant d'ajouter les valeurs correspondantes à l'utilisateur
     * @param cursor
     * @return
     */
    private User cursorToUser(Cursor cursor) {
        User user = new User();
        user.setIdUser(cursor.getInt(0));
        user.setPrenomU(cursor.getString(1));
        user.setSexe(cursor.getInt(2));
        user.setPoidsU(cursor.getInt(3));
        user.setTaille(cursor.getInt(4));
        user.setObjectif(cursor.getInt(5));
        user.setDate_inscription(cursor.getString(6));
        user.setXp(cursor.getInt(7));
        user.setLevel(cursor.getInt(8));
        return user;
    }

    /**
     * Permet de modifier le prénom de l'utilisateur dans la base de données
     * @param first_name
     */
    public void updateUserFirstName(String first_name){
        String strSQLName = "UPDATE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_USER +
                " SET " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_FIRST_NAME + " = " + "'" + first_name + "'" +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_ID + " = 1;";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de modifier le sexe de l'utilisateur dans la base de données
     * @param sexe
     */
    public void updateUserSexe(int sexe){
        String strSQLName = "UPDATE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_USER +
                " SET " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_SEXE + " = " + "'" + sexe + "'" +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_ID + " = 1;";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de modifier la taille de l'utilisateur dans la base de données
     * @param taille
     */
    public void updateUserTaille(int taille){
        String strSQLName = "UPDATE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_USER +
                " SET " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_TAILLE + " = " + "'" + taille + "'" +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_ID + " = 1;";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de modifier le poids de l'utilisateur dans la base de données
     * @param poids
     */
    public void updateUserPoids(int poids){
        String strSQLName = "UPDATE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_USER +
                " SET " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_POIDS + " = " + "'" + poids + "'" +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_ID + " = 1;";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de modifier l'objectif de l'utilisateur dans la base de données
     * @param objectif
     */
    public void updateUserObjectif(int objectif){
        String strSQLName = "UPDATE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_USER +
                " SET " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_OBJECTIF + " = " + "'" + objectif + "'" +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_ID + " = 1;";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de modifier l'xp de l'utilisateur dans la base de données
     * @param xp
     */
    public void updateUserXP(int xp){
        String strSQLName = "UPDATE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_USER +
                " SET " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_XP + " = " + "'" + xp + "'" +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_ID + " = 1;";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de modifier le niveau de l'utilisateur dans la base de données
     * @param level
     */
    public void updateUserLevel(int level){
        String strSQLName = "UPDATE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_USER +
                " SET " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_LEVEL + " = " + "'" + level + "'" +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_USER_ID + " = 1;";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de créer une famille d'exercices dans la base de données
     * @param libelle
     */
    public void createFamilleExercices(String libelle){
        ContentValues values = new ContentValues();
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_FAMILLE_EXERCICES_LIBELLE, libelle);
        database.insert(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_FAMILLE_EXERCICES, null, values);
    }

    /**
     * Permet de recupérer les familles d'exercices dans la base de données
     * @return
     */
    public List<FamilleExercices> getFamilleExercices() {
        List<FamilleExercices> list = new ArrayList<FamilleExercices>();

        Cursor cursor = database.query(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_FAMILLE_EXERCICES,
                allColumnsFamilleExercices, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            FamilleExercices familleExercices = cursorToFamilleExercices(cursor);
            list.add(familleExercices);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    /**
     * Cursor pour que, à chaque ligne récupérer avec le SELECT, récupère les valeurs
     * @param cursor
     * @return
     */
    private FamilleExercices cursorToFamilleExercices(Cursor cursor) {
        FamilleExercices familleExercices = new FamilleExercices();
        familleExercices.setId(cursor.getInt(0));
        familleExercices.setLibelle(cursor.getString(1));
        return familleExercices;
    }


    /**
     * Permet de créer un exercice
     * @param libelle
     */
    public void createExercice(String libelle, String path_image, String description, int id_famille){
        ContentValues values = new ContentValues();
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_EXERCICES_LIBELLE, libelle);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_EXERCICES_IMAGE, path_image);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_EXERCICES_DESCRIPTION, description);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_EXERCICES_ID_FAMILLE, id_famille);
        database.insert(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_EXERCICES, null, values);
    }

    /**
     * Permet de récupérer les données des exercices depuis la base de données
     * @return
     */
    public List<Exercices> getExercices() {
        List<Exercices> list = new ArrayList<Exercices>();

        Cursor cursor = database.query(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_EXERCICES,
                allColumnsExercices, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Exercices exercices = cursorToExercices(cursor);
            list.add(exercices);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    /**
     * Cursor pour que, à chaque ligne récupérer avec le SELECT, récupère les valeurs
     * @param cursor
     * @return
     */
    private Exercices cursorToExercices(Cursor cursor) {
        Exercices exercices = new Exercices();
        exercices.setId(cursor.getInt(0));
        exercices.setLibelle(cursor.getString(1));
        exercices.setPath_image(cursor.getString(2));
        exercices.setDescription(cursor.getString(3));
        exercices.setId_famille(cursor.getInt(4));
        return exercices;
    }

    /**
     * Permet de supprimer un exercice dans la base de données
     * @param id
     */
    public void deleteExercice(int id){
        String strSQLName = "DELETE FROM " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_EXERCICES +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_EXERCICES_ID + " = " + "'" + id + "';";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de modifier un exercice dans la base de données
     * @param id
     * @param libelle
     * @param family
     */
    public void updateExercice(int id, String libelle, int family){
        String strSQLName = "UPDATE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_EXERCICES +
                " SET " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_EXERCICES_LIBELLE + " = " + "'" + libelle + "'," +
                com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_EXERCICES_ID_FAMILLE + " = " + "'" + family + "'" +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_EXERCICES_ID + " = " + id + ";";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de créer une séance dans la base de données
     * @param libelle
     * @param description
     * @param is_predef
     */
    public void createSeance(String libelle, String description, int is_predef){
        ContentValues values = new ContentValues();
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_LIBELLE, libelle);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_DESCRIPTION, description);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_IS_PREDEF, is_predef);
        database.insert(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_SEANCES, null, values);
    }

    /**
     * Permet de récupérer les informations des séances dans la base de données
     * @return
     */
    public List<Seances> getSeances() {
        List<Seances> list = new ArrayList<Seances>();

        Cursor cursor = database.query(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_SEANCES,
                allColumnsSeances, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Seances seances = cursorToSeances(cursor);
            list.add(seances);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    /**
     * Permet de retourner les informations de la dernière séance dans la base de données
     * @return
     */
    public Seances getLastSeance(){
        Seances seances = new Seances();
        Cursor cursor = database.query(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_SEANCES,
                allColumnsSeances, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            seances = cursorToSeances(cursor);
            cursor.moveToNext();
        }
        cursor.close();
        return seances;
    }

    /**
     * Cursor pour que, à chaque ligne récupérer avec le SELECT, récupère les valeurs
     * @param cursor
     * @return
     */
    private Seances cursorToSeances(Cursor cursor) {
        Seances seances = new Seances();
        seances.setId(cursor.getInt(0));
        seances.setLibelle(cursor.getString(1));
        seances.setDescription(cursor.getString(2));
        seances.setIs_predef(cursor.getInt(3));
        return seances;
    }

    /**
     * Permet de supprimer une séance dans la base de données
     * @param id
     */
    public void deleteSeance(int id){
        String strSQLName = "DELETE FROM " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_SEANCES +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_ID + " = " + "'" + id + "';";
        database.execSQL(strSQLName);
    }

    /**
     * Permet d'ajouter un exercice à une séance, en indiquant le nombre de série, de répétition et le temps de repos
     * entre chaque série, dans la base de données
     * @param id_exercice
     * @param id_seance
     * @param repetitions
     * @param series
     * @param temps_repos
     */
    public void addExerciceToSeance(int id_exercice, int id_seance, int repetitions, int series, int temps_repos){
        ContentValues values = new ContentValues();
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID_EXERCICE, id_exercice);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID_SEANCE, id_seance);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_NB_REPETITIONS, repetitions);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_NB_SERIES, series);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_TEMPS_REPOS, temps_repos);
        database.insert(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_SEANCES_EXERCICES, null, values);
    }

    /**
     * Permet de récupérer les informations des exercices présent dans une séance dans la base de données
     * @param id_seance
     * @return
     */
    public List<ExericeSeance> getExercicesInSeance(int id_seance) {
        List<ExericeSeance> list = new ArrayList<ExericeSeance>();

        /*Cursor cursor = database.query(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_EXERCICES,
                allColumnsExercices, null, null, null, null, null);*/

        String request = "SELECT " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID_EXERCICE +
                ", " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_NB_REPETITIONS +
                ", " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_NB_SERIES +
                ", " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_TEMPS_REPOS +
                " FROM " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_SEANCES_EXERCICES +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID_SEANCE + " = ?";
        Cursor cursor = database.rawQuery(request, new String[] {id_seance+""});

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            Log.d("CURSOR", cursor.getString(0));
            String id_exercice = cursor.getString(0);

            String request_exo = "SELECT * FROM " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_EXERCICES +
                    " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_EXERCICES_ID + " = ?";
            Cursor cursor2 = database.rawQuery(request_exo, new String[] {id_exercice});

            cursor2.moveToFirst();
            while (!cursor2.isAfterLast()) {
                ExericeSeance exericeSeance = cursorToExercicesInSeance(cursor2);
                exericeSeance.setNb_repetition(cursor.getInt(1));
                exericeSeance.setNb_serie(cursor.getInt(2));
                exericeSeance.setTemps_repos(cursor.getInt(3));
                list.add(exericeSeance);
                cursor2.moveToNext();
            }
            cursor2.close();
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    /**
     * Cursor pour que, à chaque ligne récupérer avec le SELECT, récupère les valeurs
     * @param cursor
     * @return
     */
    private ExericeSeance cursorToExercicesInSeance(Cursor cursor) {
        ExericeSeance exericeSeance = new ExericeSeance();
        exericeSeance.setId(cursor.getInt(0));
        exericeSeance.setLibelle(cursor.getString(1));
        exericeSeance.setPath_image(cursor.getString(2));
        exericeSeance.setDescription(cursor.getString(3));
        exericeSeance.setId_famille(cursor.getInt(4));
        return exericeSeance;
    }

    /**
     * Permet de supprimer les exercices présents dans une séance
     * @param id_seance
     */
    public void deleteExercicesInSeance(int id_seance){
        String strSQLName = "DELETE FROM " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_SEANCES_EXERCICES +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID_SEANCE + " = " + "'" + id_seance + "';";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de supprimer un seul exercice dans une séance donnée
     * @param id_seance
     * @param id_exercice
     */
    public void deleteOneExerciceInSeance(int id_seance, int id_exercice){
        String strSQLName = "DELETE FROM " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_SEANCES_EXERCICES +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID_SEANCE + " = " + "'" + id_seance + "'" +
                " AND " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID_EXERCICE + " = " + "'" + id_exercice + "';";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de modifier un exercice dans une séance donnée
     * @param id_seance
     * @param id_exercice
     * @param nb_series
     * @param nb_reps
     * @param temps_repos
     */
    public void updateOneExerciceInSeance(int id_seance, int id_exercice, int nb_series, int nb_reps, int temps_repos){
        String strSQLName = "UPDATE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_SEANCES_EXERCICES +
                " SET " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_NB_SERIES + " = " + "'" + nb_series + "'," +
                com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_NB_REPETITIONS + " = " + "'" + nb_reps + "'," +
                com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_TEMPS_REPOS + " = " + "'" + temps_repos + "'" +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID_SEANCE + " = " + id_seance +
                " AND " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_SEANCES_EXERCICES_ID_EXERCICE + " = " + id_exercice + ";";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de créer une famille de recette dans la base de données
     * @param libelle
     */
    public void createFamilleRecettes(String libelle){
        ContentValues values = new ContentValues();
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_FAMILLE_RECETTES_LIBELLE, libelle);
        database.insert(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_FAMILLE_RECETTES, null, values);
    }

    /**
     * Permet de recupérer les familles de recettes dans la base de données
     * @return
     */
    public List<FamilleRecettes> getFamilleRecettes() {
        List<FamilleRecettes> list = new ArrayList<FamilleRecettes>();

        Cursor cursor = database.query(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_FAMILLE_RECETTES,
                getAllColumnsFamilleRecettes, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            FamilleRecettes familleRecettes = cursorToFamilleRecettes(cursor);
            list.add(familleRecettes);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    /**
     * Cursor pour que, à chaque ligne récupérer avec le SELECT, récupère les valeurs
     * @param cursor
     * @return
     */
    private FamilleRecettes cursorToFamilleRecettes(Cursor cursor) {
        FamilleRecettes familleRecettes = new FamilleRecettes();
        familleRecettes.setId(cursor.getInt(0));
        familleRecettes.setLibelle(cursor.getString(1));
        return familleRecettes;
    }

    /**
     * Permet de créer une recette
     * @param libelle
     */
    public void createRecette(String libelle, String path_image, String ingredients, String preparation,int id_famille){
        ContentValues values = new ContentValues();
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_RECETTES_LIBELLE, libelle);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_RECETTES_IMAGE, path_image);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_RECETTES_INGREDIENTS, ingredients);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_RECETTES_PREPARATION, preparation);
        values.put(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_RECETTES_ID_FAMILLE, id_famille);
        database.insert(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_RECETTES, null, values);
    }

    /**
     * Permet de récupérer les données des exercices depuis la base de données
     * @return
     */
    public List<Recettes> getRecettes() {
        List<Recettes> list = new ArrayList<Recettes>();

        Cursor cursor = database.query(com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_RECETTES,
                allColumnsRecettes, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Recettes recettes = cursorToRecettes(cursor);
            list.add(recettes);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    /**
     * Cursor pour que, à chaque ligne récupérer avec le SELECT, récupère les valeurs
     * @param cursor
     * @return
     */
    private Recettes cursorToRecettes(Cursor cursor) {
        Recettes recettes = new Recettes();
        recettes.setId(cursor.getInt(0));
        recettes.setLibelle(cursor.getString(1));
        recettes.setPath_image(cursor.getString(2));
        recettes.setIngredients(cursor.getString(3));
        recettes.setPreparation(cursor.getString(4));
        recettes.setId_famille(cursor.getInt(5));
        return recettes;
    }

    /**
     * Permet de supprimer un exercice dans la base de données
     * @param id
     */
    public void deleteRecette(int id){
        String strSQLName = "DELETE FROM " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_RECETTES +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_RECETTES_ID + " = " + "'" + id + "';";
        database.execSQL(strSQLName);
    }

    /**
     * Permet de modifier un exercice dans la base de données
     * @param id
     * @param libelle
     * @param family
     */
    public void updateRecette(int id, String libelle, int family){
        String strSQLName = "UPDATE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.TABLE_RECETTES +
                " SET " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_RECETTES_LIBELLE + " = " + "'" + libelle + "'," +
                com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_RECETTES_ID_FAMILLE + " = " + "'" + family + "'" +
                " WHERE " + com.example.philippe.moveandeat.Utils.SQLiteOpenHelper.COLUMN_RECETTES_ID + " = " + id + ";";
        database.execSQL(strSQLName);
    }
}
