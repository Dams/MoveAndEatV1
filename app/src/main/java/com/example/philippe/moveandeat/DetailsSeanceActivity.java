package com.example.philippe.moveandeat;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Fragments.MesSeancesFragment;
import com.example.philippe.moveandeat.Object.Exercices;
import com.example.philippe.moveandeat.Object.ExericeSeance;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DetailsSeanceActivity extends AppCompatActivity {

    private Toolbar Toolbar_DetailsSeance;
    private String name_seance;
    private int id_seance;
    private ListView List_DetailsSeance;
    private UserDataService userDataService;
    private List<ExericeSeance> list_exercices = new ArrayList<ExericeSeance>();
    private FloatingActionButton FAB_AddExerciceInSeance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_seance);

        Intent intent = getIntent();
        name_seance = intent.getStringExtra("name");
        id_seance = intent.getIntExtra("id", 0);

        Toolbar_DetailsSeance = (Toolbar) findViewById(R.id.Toolbar_DetailsSeance);
        List_DetailsSeance = (ListView) findViewById(R.id.List_DetailsSeance);
        FAB_AddExerciceInSeance = (FloatingActionButton) findViewById(R.id.FAB_AddExerciceInSeance);
        Toolbar_DetailsSeance.setTitle(name_seance);
        Toolbar_DetailsSeance.setTitleTextColor(Color.rgb(255, 255, 255));
        setSupportActionBar(Toolbar_DetailsSeance);

        //Récupération de la liste des exercices de la séance
        userDataService = new UserDataService(DetailsSeanceActivity.this);
        userDataService.open();
        list_exercices = userDataService.getExercicesInSeance(id_seance);
        userDataService.close();

        Adapter_ListExercices adapter_listExercices = new Adapter_ListExercices(list_exercices, DetailsSeanceActivity.this);
        List_DetailsSeance.setAdapter(adapter_listExercices);

        List_DetailsSeance.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent1 = new Intent(DetailsSeanceActivity.this, UpdateExericeInSeanceActivity.class);
                intent1.putExtra("id_seance", id_seance);
                intent1.putExtra("id_exercice", list_exercices.get(position).getId());
                intent1.putExtra("nb_serie", list_exercices.get(position).getNb_serie());
                intent1.putExtra("nb_reps", list_exercices.get(position).getNb_repetition());
                intent1.putExtra("temps_repos", list_exercices.get(position).getTemps_repos());
                startActivity(intent1);
            }
        });

        List_DetailsSeance.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder builder = new AlertDialog.Builder(DetailsSeanceActivity.this);
                builder.setTitle("Attention");
                builder.setMessage("Voulez-vous vraiment supprimer cet exercice de la séance ?");
                builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        userDataService = new UserDataService(DetailsSeanceActivity.this);
                        userDataService.open();
                        //Suppression de l'exercice dans la séance
                        userDataService.deleteOneExerciceInSeance(id_seance, list_exercices.get(position).getId());
                        userDataService.close();

                        //Rechargement de la liste
                        userDataService = new UserDataService(DetailsSeanceActivity.this);
                        userDataService.open();
                        list_exercices = userDataService.getExercicesInSeance(id_seance);
                        userDataService.close();
                        Adapter_ListExercices adapter_listExercices = new Adapter_ListExercices(list_exercices, DetailsSeanceActivity.this);
                        List_DetailsSeance.setAdapter(adapter_listExercices);

                    }
                });
                builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            }
        });


        FAB_AddExerciceInSeance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DetailsSeanceActivity.this, AddExercicesInSeanceActivity.class);
                intent1.putExtra("id_seance", id_seance);
                startActivity(intent1);
            }
        });

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        //Récupération de la liste des exercices de la séance
        userDataService = new UserDataService(DetailsSeanceActivity.this);
        userDataService.open();
        list_exercices = userDataService.getExercicesInSeance(id_seance);
        userDataService.close();

        Adapter_ListExercices adapter_listExercices = new Adapter_ListExercices(list_exercices, DetailsSeanceActivity.this);
        List_DetailsSeance.setAdapter(adapter_listExercices);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private class Adapter_ListExercices extends BaseAdapter {

        private List<ExericeSeance> list_exercices;
        private Activity activity;

        public Adapter_ListExercices(List<ExericeSeance> _list_exercices, Activity activity) {
            this.list_exercices = _list_exercices;
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return list_exercices.size();
        }

        @Override
        public Object getItem(int position) {
            return this.list_exercices.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {

            if(convertView == null){
                LayoutInflater layoutInflater = (LayoutInflater) activity.getApplicationContext().getSystemService(activity.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.adapter_list_exercices, parent, false);
            }

            TextView TXT_AdapterListExercice_Name = (TextView) convertView.findViewById(R.id.TXT_AdapterListExercice_Name);
            TextView TXT_AdapterListExerciceRepSerie = (TextView) convertView.findViewById(R.id.TXT_AdapterListExerciceRepSerie);
            TextView TXT_AdapterListExerciceRepos = (TextView) convertView.findViewById(R.id.TXT_AdapterListExerciceRepos);
            ImageView IMG_AdapterListExerice = (ImageView) convertView.findViewById(R.id.IMG_AdapterListExerice);

            TXT_AdapterListExercice_Name.setText(list_exercices.get(position).getLibelle());
            TXT_AdapterListExerciceRepSerie.setText(list_exercices.get(position).getNb_serie() + " série(s) de " + list_exercices.get(position).getNb_repetition() + " reps");
            TXT_AdapterListExerciceRepos.setText(list_exercices.get(position).getTemps_repos() + "'' de repos par série");

            if(!list_exercices.get(position).getPath_image().equals("") && !list_exercices.get(position).getPath_image().equals(null)){
                String uri = "@drawable/" + list_exercices.get(position).getPath_image();
                int imageResource = getResources().getIdentifier(uri, null, getPackageName());
                Drawable res = getResources().getDrawable(imageResource);
                IMG_AdapterListExerice.setImageDrawable(res);
            }else{
                Picasso.with(DetailsSeanceActivity.this).load(R.drawable.crunch).into(IMG_AdapterListExerice);
            }


            return convertView;
        }
    }

}
