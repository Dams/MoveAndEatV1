package com.example.philippe.moveandeat.Fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.philippe.moveandeat.PagerAdapter.PagerAdapter_Exercices;
import com.example.philippe.moveandeat.R;

public class ExercicesFragment extends Fragment {

    private TabLayout TabLayout_Exercices;
    private ViewPager ViewPager_Exercices;
    private PagerAdapter pagerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.exercices_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TabLayout_Exercices = (TabLayout) getActivity().findViewById(R.id.TabLayout_Exercices);
        ViewPager_Exercices = (ViewPager) getActivity().findViewById(R.id.ViewPager_Exercices);

        /* Ajout des différentes TAB */
        TabLayout_Exercices.addTab(TabLayout_Exercices.newTab().setText("Mes séances"));
        TabLayout_Exercices.addTab(TabLayout_Exercices.newTab().setText("Tous les exercices"));
        TabLayout_Exercices.setTabGravity(TabLayout.GRAVITY_FILL);

        /* Pager Adapter */
        pagerAdapter = new PagerAdapter_Exercices(getActivity().getSupportFragmentManager(), TabLayout_Exercices.getTabCount());
        ViewPager_Exercices.setAdapter(pagerAdapter);
        ViewPager_Exercices.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(TabLayout_Exercices));

        TabLayout_Exercices.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                ViewPager_Exercices.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



    }

}
