package com.example.philippe.moveandeat.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.philippe.moveandeat.AddExercicesActivity;
import com.example.philippe.moveandeat.AddSeanceActivity;
import com.example.philippe.moveandeat.BeginSeanceActivity;
import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.DetailsSeanceActivity;
import com.example.philippe.moveandeat.GenerateSeanceActivity;
import com.example.philippe.moveandeat.Object.Exercices;
import com.example.philippe.moveandeat.Object.ExericeSeance;
import com.example.philippe.moveandeat.Object.FamilleExercices;
import com.example.philippe.moveandeat.Object.Seances;
import com.example.philippe.moveandeat.R;
import com.example.philippe.moveandeat.UpdateExervicesActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MesSeancesFragment extends Fragment {

    private ExpandableListSeancesAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<Seances>> listDataChild;
    private com.getbase.floatingactionbutton.FloatingActionButton FAB_Seance, FAB_GenerateSeance;
    private UserDataService userDataService;
    private List<ExericeSeance> exericeSeanceList = new ArrayList<ExericeSeance>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mes_seances_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        expListView = (ExpandableListView) getActivity().findViewById(R.id.lvExpSeance);
        FAB_Seance = (com.getbase.floatingactionbutton.FloatingActionButton) getActivity().findViewById(R.id.FAB_Seance);
        FAB_GenerateSeance = (com.getbase.floatingactionbutton.FloatingActionButton) getActivity().findViewById(R.id.FAB_GenerateSeance);
        prepareListData();
        listAdapter = new ExpandableListSeancesAdapter(getContext(), listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {

            }
        });

        FAB_Seance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddSeanceActivity.class));
            }
        });

        FAB_GenerateSeance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), GenerateSeanceActivity.class));
            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                Seances seances = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);

                Intent intent = new Intent(getActivity(), DetailsSeanceActivity.class);
                intent.putExtra("id", seances.getId());
                intent.putExtra("name", seances.getLibelle());

                startActivity(intent);
                return false;
            }
        });

        expListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    int groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    int childPosition = ExpandableListView.getPackedPositionChild(id);

                    //Récupération des informations de l'élément enfant
                    final Seances seance = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Attention");
                    builder.setMessage("Voulez-vous vraiment supprimer la séance ? ");
                    builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            userDataService = new UserDataService(getContext());
                            userDataService.open();
                            //Suppression dans la table pivot
                            userDataService.deleteExercicesInSeance(seance.getId());
                            //Suppression de la séance
                            userDataService.deleteSeance(seance.getId());
                            userDataService.close();

                            //Rechargement de la liste
                            prepareListData();
                            listAdapter = new ExpandableListSeancesAdapter(getContext(), listDataHeader, listDataChild);
                            expListView.setAdapter(listAdapter);

                        }
                    });
                    builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();

                    return true;
                }

                return false;
            }
        });

    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Seances>>();

        listDataHeader.add("Séances prédéfinies");
        listDataHeader.add("Mes séances");

        //Récupération des séances
        UserDataService userDataService = new UserDataService(getContext());
        userDataService.open();
        List<Seances> list_seances = userDataService.getSeances();
        userDataService.close();


        List<Seances> list_seances_predefinies = new ArrayList<Seances>();
        List<Seances> list_mes_seances = new ArrayList<Seances>();

        //Parcours de tous les exercices
        for (int i = 0; i < list_seances.size(); i++){
            switch (list_seances.get(i).getIs_predef()){
                case 0:
                    list_mes_seances.add(list_seances.get(i));
                    break;
                case 1:
                    list_seances_predefinies.add(list_seances.get(i));
                    break;
                default:
                    break;
            }
        }

        listDataChild.put(listDataHeader.get(0), list_seances_predefinies);
        listDataChild.put(listDataHeader.get(1), list_mes_seances);
    }

    /**
     * Adapter de la liste étendu des séances de l'utilisateur
     */
    private class ExpandableListSeancesAdapter extends BaseExpandableListAdapter {

        private Context _context;
        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, List<Seances>> _listDataChild;

        public ExpandableListSeancesAdapter(Context context, List<String> listDataHeader,
                                            HashMap<String, List<Seances>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        /*final String childText = (String) getChild(groupPosition, childPosition);*/
            final Seances child = (Seances) getChild(groupPosition, childPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item_seance, null);
            }

            TextView lblListItemSeance = (TextView) convertView.findViewById(R.id.lblListItemSeance);
            TextView lblListItemSeanceDescription = (TextView) convertView.findViewById(R.id.lblListItemSeanceDescription);
            ImageView IMG_lblListItemSeance = (ImageView) convertView.findViewById(R.id.IMG_lblListItemSeance);

            lblListItemSeance.setText(child.getLibelle());
            lblListItemSeanceDescription.setText(child.getDescription());

            IMG_lblListItemSeance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Si la séance ne contient pas d'exerice, un message d'erreur apparait
                    userDataService = new UserDataService(getContext());
                    userDataService.open();
                    exericeSeanceList = userDataService.getExercicesInSeance(child.getId());
                    userDataService.close();

                    if(exericeSeanceList.size() == 0){
                        Toast.makeText(_context, "Il faut au moins un exercice dans une séance pour la lancer.", Toast.LENGTH_SHORT).show();
                    }else{
                        Intent intent = new Intent(getActivity(), BeginSeanceActivity.class);
                        intent.putExtra("id", child.getId());
                        intent.putExtra("name", child.getLibelle());
                        startActivity(intent);
                    }

                }
            });


            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this._listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {

            String headerTitle = (String) getGroup(groupPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group_seance, null);
            }

            TextView lblListHeaderSeance = (TextView) convertView
                    .findViewById(R.id.lblListHeaderSeance);
            lblListHeaderSeance.setTypeface(null, Typeface.BOLD);
            lblListHeaderSeance.setText(headerTitle);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }



    }




}
