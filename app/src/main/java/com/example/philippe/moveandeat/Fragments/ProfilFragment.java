package com.example.philippe.moveandeat.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Object.User;
import com.example.philippe.moveandeat.R;
import com.example.philippe.moveandeat.RecompensesActivity;
import com.example.philippe.moveandeat.Utils.Utils;
import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;

import java.text.SimpleDateFormat;

public class ProfilFragment extends Fragment {

    private LinearLayout LinearLayout_Profil_First_Name, LinearLayout_Profil_Sexe, LinearLayout_Profil_DateInscription,
            LinearLayout_Profil_Taille, LinearLayout_Profil_Poids, LinearLayout_Profil_Objectif, LinearLayout_Profil;
    private TextView TXT_Profil_Prenom, TXT_Profil_Sexe, TXT_Profil_DateInscription, TXT_Profil_Taille, TXT_Profil_Poids,
            TXT_Profil_Objectif, TXT_Profil_LevelUser;
    private ProgressBar ProgressBar_Profil;
    private UserDataService userDataService;
    private CircularFillableLoaders circularFillableLoaders;


    /*
        Le fragment est lié à un layout XML qui lui est propre
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profil_fragment, container, false);
        return view;
    }



    /*
            Action à faire quand le fragment est créer (correspond en quelque sorte au onCreate() d'une activity)
         */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout_Profil_First_Name = (LinearLayout) getActivity().findViewById(R.id.LinearLayout_Profil_First_Name);
        LinearLayout_Profil_Sexe = (LinearLayout) getActivity().findViewById(R.id.LinearLayout_Profil_Sexe);
        LinearLayout_Profil_DateInscription = (LinearLayout) getActivity().findViewById(R.id.LinearLayout_Profil_DateInscription);
        LinearLayout_Profil_Taille = (LinearLayout) getActivity().findViewById(R.id.LinearLayout_Profil_Taille);
        LinearLayout_Profil_Poids = (LinearLayout) getActivity().findViewById(R.id.LinearLayout_Profil_Poids);
        LinearLayout_Profil_Objectif = (LinearLayout) getActivity().findViewById(R.id.LinearLayout_Profil_Objectif);
        LinearLayout_Profil = (LinearLayout) getActivity().findViewById(R.id.LinearLayout_Profil);
        LinearLayout_Profil.setVisibility(View.GONE);
        TXT_Profil_Prenom = (TextView) getActivity().findViewById(R.id.TXT_Profil_Prenom);
        TXT_Profil_Sexe = (TextView) getActivity().findViewById(R.id.TXT_Profil_Sexe);
        TXT_Profil_DateInscription = (TextView) getActivity().findViewById(R.id.TXT_Profil_DateInscription);
        TXT_Profil_Taille = (TextView) getActivity().findViewById(R.id.TXT_Profil_Taille);
        TXT_Profil_Poids = (TextView) getActivity().findViewById(R.id.TXT_Profil_Poids);
        TXT_Profil_Objectif = (TextView) getActivity().findViewById(R.id.TXT_Profil_Objectif);
        TXT_Profil_LevelUser = (TextView) getActivity().findViewById(R.id.TXT_Profil_LevelUser);
        ProgressBar_Profil = (ProgressBar) getActivity().findViewById(R.id.ProgressBar_Profil);
        circularFillableLoaders = (CircularFillableLoaders) getActivity().findViewById(R.id.circularFillableLoaders);
        ProgressBar_Profil.setVisibility(View.VISIBLE);

        //Récupération des valeurs depuis la base de données
        userDataService = new UserDataService(getContext());
        userDataService.open();
        User user = userDataService.getUser();
        userDataService.close();

        //Affichage des valeurs dans les textviews
        TXT_Profil_Prenom.setText(user.getPrenomU());
        TXT_Profil_DateInscription.setText(parseDateInscription(user.getDate_inscription()));
        TXT_Profil_Taille.setText(user.getTaille()+"");
        TXT_Profil_Poids.setText(user.getPoidsU()+"");
        TXT_Profil_LevelUser.setText("Niveau " + user.getLevel() + "\n" + user.getXp() + " / " + Utils.getNextXpUser(user.getXp()) +  " xp");

        switch (user.getObjectif()){
            case 1:
                TXT_Profil_Objectif.setText("Prendre du muscle");
                break;
            case 2:
                TXT_Profil_Objectif.setText("Perdre du gras");
                break;
            case 3:
                TXT_Profil_Objectif.setText("Garder son poids");
                break;
            default:
                break;
        }

        //Modification de l'indicateur de niveau
        circularFillableLoaders.setProgress(Utils.getProgressionOfUser(user.getXp()));

        ProgressBar_Profil.setVisibility(View.GONE);
        LinearLayout_Profil.setVisibility(View.VISIBLE);

        TXT_Profil_LevelUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), RecompensesActivity.class));
            }
        });

        LinearLayout_Profil_First_Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.custom_dialog_edittext, null);
                builder.setView(dialogView);

                final EditText Custom_EditText = (EditText) dialogView.findViewById(R.id.Custom_EditText);
                Custom_EditText.setText(TXT_Profil_Prenom.getText().toString());

                builder.setTitle("Modifier votre prénom");

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TXT_Profil_Prenom.setText(Custom_EditText.getText().toString());

                        //Modification dans la base de données
                        userDataService.open();
                        userDataService.updateUserFirstName(Custom_EditText.getText().toString());
                        userDataService.close();

                    }
                });

                builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        LinearLayout_Profil_Sexe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.custom_dialog_radiobutton, null);
                builder.setView(dialogView);

                final RadioButton RadioBTN_1 = (RadioButton) dialogView.findViewById(R.id.RadioBTN_1);
                final RadioButton RadioBTN_2 = (RadioButton) dialogView.findViewById(R.id.RadioBTN_2);

                if(TXT_Profil_Sexe.getText().toString().equals("Homme")){
                    RadioBTN_1.setChecked(true);
                }else{
                    RadioBTN_2.setChecked(true);
                }

                builder.setTitle("Modifier votre sexe");

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int sexe = 0;
                        if(RadioBTN_1.isChecked()){
                            sexe = 1;
                            TXT_Profil_Sexe.setText("Homme");
                        }else{
                            sexe = 2;
                            TXT_Profil_Sexe.setText("Femme");
                        }

                        //Modification dans la base de données
                        userDataService.open();
                        userDataService.updateUserSexe(sexe);
                        userDataService.close();
                    }
                });

                builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });


        LinearLayout_Profil_Taille.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.custom_dialog_edittext_number, null);
                builder.setView(dialogView);

                final EditText Custom_EditText_Number = (EditText) dialogView.findViewById(R.id.Custom_EditText_Number);
                Custom_EditText_Number.setText(TXT_Profil_Taille.getText().toString());

                builder.setTitle("Modifier votre taille");

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TXT_Profil_Taille.setText(Custom_EditText_Number.getText().toString());

                        //Modification dans la base de données
                        userDataService.open();
                        userDataService.updateUserTaille(Integer.parseInt(Custom_EditText_Number.getText().toString()));
                        userDataService.close();

                    }
                });

                builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        LinearLayout_Profil_Poids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.custom_dialog_edittext_number, null);
                builder.setView(dialogView);

                final EditText Custom_EditText_Number = (EditText) dialogView.findViewById(R.id.Custom_EditText_Number);
                Custom_EditText_Number.setText(TXT_Profil_Poids.getText().toString());

                builder.setTitle("Modifier votre poids");

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TXT_Profil_Poids.setText(Custom_EditText_Number.getText().toString());

                        //Modification dans la base de données
                        userDataService.open();
                        userDataService.updateUserPoids(Integer.parseInt(Custom_EditText_Number.getText().toString()));
                        userDataService.close();

                    }
                });

                builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        LinearLayout_Profil_Objectif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.custom_dialog_radiobutton_2, null);
                builder.setView(dialogView);

                final RadioButton RadioBTN_Masse = (RadioButton) dialogView.findViewById(R.id.RadioBTN_Masse);
                final RadioButton RadioBTN_Gras = (RadioButton) dialogView.findViewById(R.id.RadioBTN_Gras);
                final RadioButton RadioBTN_Stable = (RadioButton) dialogView.findViewById(R.id.RadioBTN_Stable);

                if(TXT_Profil_Objectif.getText().toString().equals("Prendre du muscle")){
                    RadioBTN_Masse.setChecked(true);
                }else if(TXT_Profil_Objectif.getText().toString().equals("Perdre du gras")){
                    RadioBTN_Gras.setChecked(true);
                }else{
                    RadioBTN_Stable.setChecked(true);
                }

                builder.setTitle("Modifier votre objectif");

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int objectif = 0;
                        if(RadioBTN_Masse.isChecked()){
                            objectif = 1;
                            TXT_Profil_Objectif.setText("Prendre du muscle");
                        }else if(RadioBTN_Gras.isChecked()){
                            objectif = 2;
                            TXT_Profil_Objectif.setText("Perdre du gras");
                        }else{
                            objectif = 3;
                            TXT_Profil_Objectif.setText("Garder son poids");
                        }

                        //Modification dans la base de données
                        userDataService.open();
                        userDataService.updateUserObjectif(objectif);
                        userDataService.close();
                    }
                });

                builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    /**
     * Fonction permettant de parser la date d'inscription de l'utilisateur
     * @param date
     * @return
     */
    private String parseDateInscription(String date){
        //Date de la forme yyyy-mm-dd hh:mm:ss
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);

        return day + "/" + month + "/" + year;
    }
}
