package com.example.philippe.moveandeat.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.philippe.moveandeat.AddRecetteActivity;
import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Object.Exercices;
import com.example.philippe.moveandeat.Object.FamilleRecettes;
import com.example.philippe.moveandeat.Object.Recettes;
import com.example.philippe.moveandeat.R;
import com.example.philippe.moveandeat.UpdateExervicesActivity;
import com.example.philippe.moveandeat.UpdateRecettesActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RecettesFragments extends Fragment {

    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<Recettes>> listDataChild;
    private FloatingActionButton FAB_AllRecettes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recettes_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
    @Override
    public void onResume() {
        super.onResume();
        final UserDataService userDataService = new UserDataService(getContext());
        userDataService.open();
        final List<Recettes> list_recette_predef = userDataService.getRecettes();
        userDataService.close();
        expListView = (ExpandableListView) getActivity().findViewById(R.id.lvExpRecette);
        FAB_AllRecettes = (FloatingActionButton) getActivity().findViewById(R.id.FAB_AllRecettes);
        prepareListData();
        listAdapter = new ExpandableListAdapterRecette(getContext(), listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                Recettes recettes = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                Intent intent = new Intent(getActivity(), UpdateRecettesActivity.class);

                intent.putExtra("id", recettes.getId());
                intent.putExtra("libelle", recettes.getLibelle());
                intent.putExtra("image", recettes.getPath_image());
                intent.putExtra("ingredient", recettes.getIngredients());
                intent.putExtra("preparation", recettes.getPreparation());

                startActivity(intent);
                return false;
            }
        });


        //Quand l'utilisateur appuis longtemps sur un élément enfant de la liste
        expListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    int groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    int childPosition = ExpandableListView.getPackedPositionChild(id);

                    //Récupération des informations de l'élément enfant
                    final Recettes recettes = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Attention");
                    builder.setMessage("Voulez-vous vraiment supprimer la recette " + recettes.getLibelle() + " ?");
                    builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            userDataService.open();
                            userDataService.deleteRecette(recettes.getId());
                            userDataService.close();

                            //Rechargement de la liste
                            prepareListData();
                            listAdapter = new ExpandableListAdapterRecette(getContext(), listDataHeader, listDataChild);
                            expListView.setAdapter(listAdapter);

                        }
                    });
                    builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();

                    return true;
                }
                return false;
            }
        });

       FAB_AllRecettes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddRecetteActivity.class));
            }
        });
    }
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Recettes>>();

        //Récupération des familles de recettes
        UserDataService userDataService = new UserDataService(getContext());
        userDataService.open();
        List<FamilleRecettes> list = userDataService.getFamilleRecettes();
        userDataService.close();

        //Ajout des noms des familles de recettes dans la header list
        for (int i = 0; i < list.size(); i++) {
            listDataHeader.add(list.get(i).getLibelle());
        }

        //Récupération des recettes
        userDataService.open();
        List<Recettes> list_recettes = userDataService.getRecettes();
        userDataService.close();


        List<Recettes> list_recette_matin = new ArrayList<Recettes>();
        List<Recettes> list_recette_midi = new ArrayList<Recettes>();
        List<Recettes> list_recette_soir = new ArrayList<Recettes>();
        List<Recettes> list_recette_snacks = new ArrayList<Recettes>();

        //Parcours de toutes les recettes
        for (int i = 0; i < list_recettes.size(); i++) {
            switch (list_recettes.get(i).getId_famille()) {
                case 1:
                    list_recette_matin.add(list_recettes.get(i));
                    break;
                case 2:
                    list_recette_midi.add(list_recettes.get(i));
                    break;
                case 3:
                    list_recette_soir.add(list_recettes.get(i));
                    break;
                case 4:
                    list_recette_snacks.add(list_recettes.get(i));
                    break;

                default:
                    break;
            }
        }

        listDataChild.put(listDataHeader.get(0), list_recette_matin); // Header, Child data
        listDataChild.put(listDataHeader.get(1), list_recette_midi);
        listDataChild.put(listDataHeader.get(2), list_recette_soir);
        listDataChild.put(listDataHeader.get(3), list_recette_snacks);
    }
    private class ExpandableListAdapterRecette extends BaseExpandableListAdapter {

        private Context _context;
        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, List<Recettes>> _listDataChild;

        public ExpandableListAdapterRecette(Context context, List<String> listDataHeader,
                                     HashMap<String, List<Recettes>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        /*final String childText = (String) getChild(groupPosition, childPosition);*/
            Recettes child = (Recettes) getChild(groupPosition, childPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item, null);
            }

            TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
            ImageView IMG_Exercice = (ImageView) convertView.findViewById(R.id.IMG_Exercice);

            txtListChild.setText(child.getLibelle());
            if(!child.getPath_image().equals("") && !child.getPath_image().equals(null)){
                String uri = "@drawable/" + child.getPath_image();
                int imageResource = _context.getResources().getIdentifier(uri, null, _context.getPackageName());
                Drawable res = _context.getResources().getDrawable(imageResource);
                IMG_Exercice.setImageDrawable(res);
            }else{
                Picasso.with(getContext()).load(R.drawable.badge_1).into(IMG_Exercice);
            }

            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this._listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {

            String headerTitle = (String) getGroup(groupPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }

            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);
            lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }


    }
}
