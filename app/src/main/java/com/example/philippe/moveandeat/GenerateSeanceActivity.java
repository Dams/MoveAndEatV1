package com.example.philippe.moveandeat;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Interfaces.LoadDataComplete;
import com.example.philippe.moveandeat.Object.Seances;

import java.util.ArrayList;
import java.util.Random;

public class GenerateSeanceActivity extends AppCompatActivity implements LoadDataComplete{

    private Toolbar Toolbar_GenerateSeance;
    private EditText EditTXT_GenerateSeance_Name, EditTXT_NumberExercicesGenerates;
    private CheckBox CheckBox_Epaules, CheckBox_Pectoraux, CheckBox_Biceps, CheckBox_AvantBras, CheckBox_Abdos, CheckBox_Trapezes,
            CheckBox_Dos, CheckBox_Triceps, CheckBox_Lombaires, CheckBox_Fessiers, CheckBox_Quadriceps, CheckBox_Mollets;
    private Button BTN_GenerateSeance;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_seance);

        Toolbar_GenerateSeance = (Toolbar) findViewById(R.id.Toolbar_GenerateSeance);
        Toolbar_GenerateSeance.setTitle("Générer une séance");
        Toolbar_GenerateSeance.setTitleTextColor(Color.rgb(255, 255, 255));
        setSupportActionBar(Toolbar_GenerateSeance);
        EditTXT_GenerateSeance_Name = (EditText) findViewById(R.id.EditTXT_GenerateSeance_Name);
        EditTXT_NumberExercicesGenerates = (EditText) findViewById(R.id.EditTXT_NumberExercicesGenerates);
        CheckBox_Epaules = (CheckBox) findViewById(R.id.CheckBox_Epaules);
        CheckBox_Pectoraux = (CheckBox) findViewById(R.id.CheckBox_Pectoraux);
        CheckBox_Biceps = (CheckBox) findViewById(R.id.CheckBox_Biceps);
        CheckBox_AvantBras = (CheckBox) findViewById(R.id.CheckBox_AvantBras);
        CheckBox_Abdos = (CheckBox) findViewById(R.id.CheckBox_Abdos);
        CheckBox_Trapezes = (CheckBox) findViewById(R.id.CheckBox_Trapezes);
        CheckBox_Dos = (CheckBox) findViewById(R.id.CheckBox_Dos);
        CheckBox_Triceps = (CheckBox) findViewById(R.id.CheckBox_Triceps);
        CheckBox_Lombaires = (CheckBox) findViewById(R.id.CheckBox_Lombaires);
        CheckBox_Fessiers = (CheckBox) findViewById(R.id.CheckBox_Epaules);
        CheckBox_Quadriceps = (CheckBox) findViewById(R.id.CheckBox_Quadriceps);
        CheckBox_Mollets = (CheckBox) findViewById(R.id.CheckBox_Mollets);
        BTN_GenerateSeance = (Button) findViewById(R.id.BTN_GenerateSeance);

        BTN_GenerateSeance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!EditTXT_GenerateSeance_Name.getText().toString().equals("")){
                    if(!EditTXT_NumberExercicesGenerates.getText().toString().equals("")){
                        GenerateSeance generateSeance = new GenerateSeance(GenerateSeanceActivity.this, EditTXT_GenerateSeance_Name.getText().toString());
                        generateSeance.execute();
                    }else{
                        Toast.makeText(getApplicationContext(), "Merci d'entrer le nombre d'exercices souhaités.", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Merci d'entrer le nombre d'exercices souhaités.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Permet de générer une séance et de l'enregistrer en base de données
     */
    private void generateSeance(String name){

        int nb_exos = Integer.parseInt(EditTXT_NumberExercicesGenerates.getText().toString());
        ArrayList<Integer> list_int = new ArrayList<>();
        ArrayList<Integer> list_int_exercices = new ArrayList<>();

        if(CheckBox_Epaules.isChecked()){
            list_int.add(1);
        }
        if(CheckBox_Pectoraux.isChecked()){
            list_int.add(2);
        }
        if(CheckBox_Biceps.isChecked()){
            list_int.add(3);
        }
        if(CheckBox_AvantBras.isChecked()){
            list_int.add(4);
        }
        if(CheckBox_Abdos.isChecked()){
            list_int.add(5);
        }
        if(CheckBox_Trapezes.isChecked()){
            list_int.add(6);
        }
        if(CheckBox_Dos.isChecked()){
            list_int.add(7);
        }
        if(CheckBox_Triceps.isChecked()){
            list_int.add(8);
        }
        if(CheckBox_Lombaires.isChecked()){
            list_int.add(9);
        }
        if(CheckBox_Fessiers.isChecked()){
            list_int.add(10);
        }
        if(CheckBox_Quadriceps.isChecked()){
            list_int.add(11);
        }
        if(CheckBox_Mollets.isChecked()){
            list_int.add(12);
        }

        //Si le tableau est vide c'est que l'utilisateur n'a checker aucune checkbox
        if(list_int.size() == 0){
            /*Toast.makeText(this, "Merci de selectionner aux moins une catégorie.", Toast.LENGTH_SHORT).show();*/
            return;
        }

        /*
         *  Pour la génération, on divise le nombre d'exo demandé par le nombre de catégorie pour avoir des exos qui ne sont pas
         *  toujours dans la même catégorie et on boucle sur la liste pour ajouter des exos.
         */

        int nb_exo_per_categories = nb_exos / list_int.size();
        if(nb_exo_per_categories < 1){
            nb_exo_per_categories = 1;
        }

        Random random = new Random();
        for (int i = 0 ; i < list_int.size(); i++){
            switch (list_int.get(i)){
                //Epaules (87 - 106)
                case 1:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((106 - 87)+1) + 87);
                    }
                    break;

                //Pectoraux 136 - 159
                case 2:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((159 - 136)+1) + 136);
                    }
                    break;

                //Biceps 43 - 71
                case 3:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((71 - 43)+1) + 43);
                    }
                    break;

                //Avant Bras 25 - 42
                case 4:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((42 - 25)+1) + 25);
                    }
                    break;

                //Abdos 1 - 24
                case 5:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((24 - 1)+1) + 1);
                    }
                    break;

                //Trapezes 186 - 191
                case 6:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((191 - 186)+1) + 186);
                    }
                    break;

                //Dos 72 - 86
                case 7:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((86 - 72)+1) + 72);
                    }
                    break;

                //Triceps 192 - 216
                case 8:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((216 - 192)+1) + 192);
                    }
                    break;

                //Lombaires 123 - 127
                case 9:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((127 - 123)+1) + 123);
                    }
                    break;

                //Fessiers 107 - 114
                case 10:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((114 - 107)+1) + 107);
                    }
                    break;

                //Quadriceps 160 - 185
                case 11:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((185 - 160)+1) + 160);
                    }
                    break;

                //Mollets 128 - 135
                case 12:
                    for (int j = 0; j < nb_exo_per_categories; j++){
                        list_int_exercices.add(random.nextInt((135 - 128)+1) + 128);
                    }
                    break;

                default:
                    break;
            }

        }


        //Dans le cas ou la liste des int des exos est plus grande que le nombre d'exo
        if(list_int_exercices.size() > nb_exos){
            //On prend les x premiers exos
            for(int l = list_int_exercices.size(); l > nb_exos; l--){
                list_int_exercices.remove(l-1);
            }
        }else if(list_int_exercices.size() < nb_exos){
            //Si il manque des exos, on pioche dans la liste des familles cochés
            int nb_exo_manquant = nb_exos - list_int_exercices.size();

            for (int w = 0; w < nb_exo_manquant; w++){
                int famille = random.nextInt(list_int.size());

                switch (famille+1){
                    //Epaules (87 - 106)
                    case 1:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((106 - 87)+1) + 87);
                        }
                        break;

                    //Pectoraux 136 - 159
                    case 2:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((159 - 136)+1) + 136);
                        }
                        break;

                    //Biceps 43 - 71
                    case 3:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((71 - 43)+1) + 43);
                        }
                        break;

                    //Avant Bras 25 - 42
                    case 4:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((42 - 25)+1) + 25);
                        }
                        break;

                    //Abdos 1 - 24
                    case 5:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((24 - 1)+1) + 1);
                        }
                        break;

                    //Trapezes 186 - 191
                    case 6:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((191 - 186)+1) + 186);
                        }
                        break;

                    //Dos 72 - 86
                    case 7:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((86 - 72)+1) + 72);
                        }
                        break;

                    //Triceps 192 - 216
                    case 8:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((216 - 192)+1) + 192);
                        }
                        break;

                    //Lombaires 123 - 127
                    case 9:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((127 - 123)+1) + 123);
                        }
                        break;

                    //Fessiers 107 - 114
                    case 10:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((114 - 107)+1) + 107);
                        }
                        break;

                    //Quadriceps 160 - 185
                    case 11:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((185 - 160)+1) + 160);
                        }
                        break;

                    //Mollets 128 - 135
                    case 12:
                        for (int j = 0; j < nb_exo_per_categories; j++){
                            list_int_exercices.add(random.nextInt((135 - 128)+1) + 128);
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        UserDataService userDataService = new UserDataService(GenerateSeanceActivity.this);
        userDataService.open();
        //Création de la séance
        userDataService.createSeance(name, "", 0);
        //Récupération de l'id de la séance créer
        Seances seances = userDataService.getLastSeance();
        for (int i = 0; i < list_int_exercices.size(); i++){
            userDataService.addExerciceToSeance(list_int_exercices.get(i), seances.getId(), 15, 3, 30);
        }
        userDataService.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onLoadDataComplete() {
        finish();
    }

    public class GenerateSeance extends AsyncTask {

        private String name;
        private LoadDataComplete loadDataComplete;

        public GenerateSeance(LoadDataComplete loadDataComplete, String name) {
            this.loadDataComplete = loadDataComplete;
            this.name = name;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(GenerateSeanceActivity.this);
            progressDialog.setMessage("Génération de la séance...");
            progressDialog.setTitle("Chargement");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            generateSeance(name);
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progressDialog.dismiss();
            loadDataComplete.onLoadDataComplete();
        }
    }





}
