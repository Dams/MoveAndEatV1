package com.example.philippe.moveandeat.Interfaces;

/**
 * Created by Damian on 12/05/2017.
 */

public interface LoadDataComplete {

    void onLoadDataComplete();


}
