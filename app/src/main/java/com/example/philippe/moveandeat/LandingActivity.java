package com.example.philippe.moveandeat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Interfaces.LoadDataComplete;
import com.example.philippe.moveandeat.Object.FamilleExercices;
import com.example.philippe.moveandeat.Object.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LandingActivity extends AppCompatActivity implements LoadDataComplete{

    private ImageView IMG_Landing;
    private TextView TXT_Landing_Title;
    private Animation AnimationFadeIn;
    private LinearLayout LinearLayout_Landing;
    private TextInputLayout Input_Layout_Prenom, Input_Layout_Taille, Input_Layout_Poids;
    private EditText Input_Prenom, Input_Taille, Input_Poids;
    private Spinner SPIN_Landing;
    private Button BTN_Landing_Begin;
    private RadioButton RadioBTN_Homme, RadioBTN_Femme;
    private UserDataService userDataService;
    private int objectif = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        AnimationFadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);
        IMG_Landing = (ImageView) findViewById(R.id.IMG_Landing);
        TXT_Landing_Title = (TextView) findViewById(R.id.TXT_Landing_Title);
        LinearLayout_Landing = (LinearLayout) findViewById(R.id.LinearLayout_Landing);
        Input_Layout_Prenom = (TextInputLayout) findViewById(R.id.Input_Layout_Prenom);
        Input_Layout_Taille = (TextInputLayout) findViewById(R.id.Input_Layout_Taille);
        Input_Layout_Poids = (TextInputLayout) findViewById(R.id.Input_Layout_Poids);
        Input_Prenom = (EditText) findViewById(R.id.Input_Prenom);
        Input_Taille = (EditText) findViewById(R.id.Input_Taille);
        Input_Poids = (EditText) findViewById(R.id.Input_Poids);
        SPIN_Landing = (Spinner) findViewById(R.id.SPIN_Landing);
        BTN_Landing_Begin = (Button) findViewById(R.id.BTN_Landing_Begin);
        RadioBTN_Homme = (RadioButton) findViewById(R.id.RadioBTN_Homme);
        RadioBTN_Homme.setChecked(true);
        RadioBTN_Femme = (RadioButton) findViewById(R.id.RadioBTN_Femme);

        LinearLayout_Landing.setVisibility(View.GONE);
        IMG_Landing.setAnimation(AnimationFadeIn);
        TXT_Landing_Title.setAnimation(AnimationFadeIn);

        final List<String> objectifs = new ArrayList<String>();
        objectifs.add("Prendre du muscle");
        objectifs.add("Perdre du gras");
        objectifs.add("Garder son poids");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.simple_item_spinner, objectifs);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPIN_Landing.setAdapter(dataAdapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Vérification de la présence d'un utilisateur dans la base de données
                userDataService = new UserDataService(LandingActivity.this);
                userDataService.open();
                User user = userDataService.getUser();
                userDataService.close();

                if(user.getIdUser() == 0){
                    LinearLayout_Landing.setVisibility(View.VISIBLE);
                }else{
                    startActivity(new Intent(LandingActivity.this, AccueilActivity.class));
                    finish();
                }
            }
        }, 2000);


        SPIN_Landing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                objectif = position + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                objectif = 1;
            }
        });


        BTN_Landing_Begin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!Input_Prenom.getText().toString().equals("")){
                    if(!Input_Taille.getText().toString().equals("")){
                        if(!Input_Poids.getText().toString().equals("")){

                            int sexe = 0;
                            if(RadioBTN_Homme.isChecked()){
                                sexe = 1;
                            }else{
                                sexe = 2;
                            }

                            userDataService = new UserDataService(LandingActivity.this);
                            userDataService.open();

                            //Création de l'utilisateur
                            userDataService.createUser(Input_Prenom.getText().toString(),
                                    Integer.toString(sexe), Input_Poids.getText().toString(),
                                    Input_Taille.getText().toString(),
                                    Integer.toString(objectif), "0", 1);
                            userDataService.close();

                            LoadData loadData = new LoadData(LandingActivity.this);
                            loadData.execute();


                        }else{
                            Input_Layout_Poids.setErrorEnabled(true);
                            Input_Layout_Poids.setError("Veuillez entrer votre poids");
                        }
                    }else{
                        Input_Layout_Taille.setErrorEnabled(true);
                        Input_Layout_Taille.setError("Veuillez entrer votre taille");
                    }
                }else{
                    Input_Layout_Prenom.setErrorEnabled(true);
                    Input_Layout_Prenom.setError("Veuillez entrer votre prénom");
                }
            }
        });

    }

    /**
     * Fonction permettant de créer les familles d'exercices dans la base de données
     */
    private void createFamilleExerices(){
        userDataService = new UserDataService(LandingActivity.this);
        userDataService.open();

        userDataService.createFamilleExercices("Abdos");
        userDataService.createFamilleExercices("Avant-bras");
        userDataService.createFamilleExercices("Biceps");
        userDataService.createFamilleExercices("Dos");
        userDataService.createFamilleExercices("Epaules");
        userDataService.createFamilleExercices("Fessiers");
        userDataService.createFamilleExercices("Ischios-jambiers");
        userDataService.createFamilleExercices("Lombaires");
        userDataService.createFamilleExercices("Mollets");
        userDataService.createFamilleExercices("Pectoraux");
        userDataService.createFamilleExercices("Quadriceps");
        userDataService.createFamilleExercices("Trapèzes");
        userDataService.createFamilleExercices("Triceps");

        userDataService.close();
    }

    private void createExercices(){
        userDataService = new UserDataService(LandingActivity.this);
        userDataService.open();

        //Abdos
        userDataService.createExercice("Crunch", "crunch", "", 1); //1
        userDataService.createExercice("Flexions latérales avec haltère", "flexions_laterales_avec_haltere", "", 1);
        userDataService.createExercice("Le pont", "le_pont", "", 1);
        userDataService.createExercice("Machine à crunch", "machine_a_crunch", "", 1);
        userDataService.createExercice("Planche", "planche", "", 1);
        userDataService.createExercice("Planche latérale", "planche_laterale", "", 1);
        userDataService.createExercice("Relevé de jambes", "releve_de_jambes", "", 1);
        userDataService.createExercice("Sit-up", "sit_up", "", 1);

        //Avant-bras
        userDataService.createExercice("Curl barre pronation", "curl_barre_pronation", "", 2);
        userDataService.createExercice("Curl barre pronation à la poulie basse", "curl_barre_pronation_a_la_poulie_basse", "", 2);//10
        userDataService.createExercice("Curl du poignet à l'haltère assis", "curl_du_poignet_a_halter_assis", "", 2);
        userDataService.createExercice("Curl poignet à la barre debout (supination)", "curl_poignet_a_la_barre_debout_supination", "", 2);
        userDataService.createExercice("Curl poignet à la barre derrière le dos debout", "curl_poignet_a_la_barre_derriere_le_dos_debout", "", 2);
        userDataService.createExercice("Curl poignet à la barre EZ debout", "curl_poignet_a_la_barre_ez_debout", "", 2);

        //Biceps
        userDataService.createExercice("Curl assis haltères prise marteau", "curl_assis_halteres_marteau", "", 3);
        userDataService.createExercice("Curl assis haltères supination", "curl_assis_halteres_supination", "", 3);
        userDataService.createExercice("Curl barre EZ supination", "curl_barre_ez_supination", "", 3);
        userDataService.createExercice("Curl biceps à la machine", "curl_biceps_a_la_machine", "", 3);
        userDataService.createExercice("Curl biceps alterné", "curl_biceps_alterne", "", 3);
        userDataService.createExercice("Curl haltères pronation", "curl_haltere_pronation", "", 3);//20
        userDataService.createExercice("Curl haltères supination", "curl_haltere_supination", "", 3);
        userDataService.createExercice("Curl incliné aux haltères", "curl_incline_haltere", "", 3);
        userDataService.createExercice("Curl marteau alterné avec haltères", "curl_marteau_alterne_haltere", "", 3);
        userDataService.createExercice("Curl marteau croisé", "curl_marteau_croise", "", 3);

        //Dos
        userDataService.createExercice("Rowing haltère unilatéral", "rowing_haltere_unilaeral", "", 4);
        userDataService.createExercice("Rowing horizontal au câble", "rowing_horizontal_au_cable", "", 4);
        userDataService.createExercice("Rowing horizontal barre droite", "rowing_horizontal_barre_droite", "", 4);
        userDataService.createExercice("Soulevé de terre", "souleve_de_terre", "", 4);
        userDataService.createExercice("Tirage bras tendus debout poulie haute", "tirage_bras_tendu_debout_poulie_haute", "", 4);
        userDataService.createExercice("Tirage horizontal prise neutre serrée", "tirage_horizontal_prise_neutre_serree", "", 4);//30
        userDataService.createExercice("Tirage nuque poulie haute ", "tirage_nuque_poulie_haute", "", 4);
        userDataService.createExercice("Tirage poitrine prise large", "tirage_poitrine_prise_large", "", 4);
        userDataService.createExercice("Tractions en pronation", "tractions_en_pronation", "", 4);
        userDataService.createExercice("Tractions supination", "tractions_en_supination", "", 4);


        //Epaules
        userDataService.createExercice("Développé épaule à la machine", "developpe_epaule_a_la_machine", "", 5);
        userDataService.createExercice("Développé incliné aux haltères", "developpe_incline_aux_halteres", "", 5);
        userDataService.createExercice("Développé militaire assis barre", "developpe_militaire_assis_barre", "", 5);
        userDataService.createExercice("Développé militaire haltères", "developpe_militaire_halteres", "", 5);
        userDataService.createExercice("Elévations antérieures à la barre", "elevetaions_anterieures_a_la_barre", "", 5);
        userDataService.createExercice("Elévations antérieures avec haltères", "elevations_anterieures_avec_halteres", "", 5);//40
        userDataService.createExercice("Elévations latérales avec haltères", "elevations_laterales_avec_halteres", "", 5);
        userDataService.createExercice("Elévations latérales avec haltères penché en avant", "elevations_anterieures_avec_halteres_penchees", "", 5);
        userDataService.createExercice("Face pull", "face_pull", "", 5);

        //Fessiers
        userDataService.createExercice("Extension hanches à genoux", "extension_hanches_a_genoux", "", 6);
        userDataService.createExercice("Extension jambe droite", "extension_jambe_droite", "", 6);
        userDataService.createExercice("Fentes avant à la barre", "fentes_avant_a_la_barre", "", 6);
        userDataService.createExercice("Fentes avant avec haltères", "fentes_avant_avec_halteres", "", 6);
        userDataService.createExercice("Fentes avant haltères unilatéral", "fentes_avant_avec_halteres_unilateral", "", 6);
        userDataService.createExercice("Fentes latérales à la barre", "fentes_laterales_a_la_barre", "", 6);
        userDataService.createExercice("Kickback unilatéral à la poulie basse", "kickback_unilateral_a_la_poulie_basse", "", 6);//50
        userDataService.createExercice("Levée de jambe talon fesse", "levee_de_jambe_talon_fesse", "", 6);


        //Ischios-jambiers
        userDataService.createExercice("Battements de jambes", "battement_de_jambes", "", 7);
        userDataService.createExercice("Curl jambe assis", "curl_jambe_assis", "", 7);
        userDataService.createExercice("Curl jambe debout", "curl_jambe_debout", "", 7);
        userDataService.createExercice("Good morning barre", "good_morning_barre", "", 7);
        userDataService.createExercice("Leg curl", "leg_curl", "", 7);
        userDataService.createExercice("Soulevé de terre jambes tendues", "soumeve_de_terre_jambes_tendus", "", 7);
        userDataService.createExercice("Soulevé de terre roumain", "souleve_de_terre_roumain", "", 7);
        userDataService.createExercice("Squat Jefferson à la barre", "squat_jeffereson_a_la_barre", "", 7);

        //Lombaires
        userDataService.createExercice("Extension à la chaise avec enroulement", "extension_a_la_chaise_avec_enroulement", "", 8);//60
        userDataService.createExercice("Extension lombaire sur boule", "entension_lombaire_sur_boule", "", 8);
        userDataService.createExercice("Extensions lombaires au tirage horizontal", "entension_lombaire_au_tirage_horizontal", "", 8);
        userDataService.createExercice("Soulevé de terre aux haltères", "souleve_de_terre_aux_halteres", "", 8);
        userDataService.createExercice("Superman", "superman", "", 8);

        //Mollets
        userDataService.createExercice("Extension du mollet debout (droit)", "extension_du_mollet_droit", "", 9);
        userDataService.createExercice("Extension du mollet debout (gauche)", "extension_du_mollet_gauche", "", 9);
        userDataService.createExercice("Extension mollets à la barre assis", "extension_mollet_a_la_barre_assis", "", 9);
        userDataService.createExercice("Extension mollets à la barre debout", "extension_mollet_a_la_barre_debout", "", 9);
        userDataService.createExercice("Extension mollets à la machine assis", "extension_mollet_a_la_machine_assis", "", 9);
        userDataService.createExercice("Extension mollets à la machine debout", "extension_mollet_a_la_machine_debout", "", 9);//70
        userDataService.createExercice("Extension mollets à la Smith machine", "extension_mollet_a_la_smith_machine", "", 9);
        userDataService.createExercice("Extension mollets à la presse à cuisses", "extension_mollet_a_la_presse_a_cuisse", "", 9);

        //Pectoraux
        userDataService.createExercice("Développé couché", "developpe_couche", "", 10);
        userDataService.createExercice("Développé couché avec haltères", "developpe_couche_avec_halteres", "", 10);
        userDataService.createExercice("Développé décliné avec haltères", "developpe_decline_avec_halteres", "", 10);
        userDataService.createExercice("Développé incliné avec haltères", "developpe_incline_avec_halteres", "", 10);
        userDataService.createExercice("Développé incliné barre", "developpe_incline_avec_barre", "", 10);
        userDataService.createExercice("Développé incliné prise marteau", "developpe_incline_prise_marteau", "", 10);
        userDataService.createExercice("Ecarté incliné avec haltères", "ecarte_incline_avec_halteres", "", 10);
        userDataService.createExercice("Ecarté à la machine", "ecarte_a_la_machine", "", 10);//80
        userDataService.createExercice("Ecarté à la poulie vis-à-vis", "ecarte_a_la_poulie_vis_a_vis", "", 10);
        userDataService.createExercice("Ecarté couchés avec haltères", "ecarte_couches_avec_halteres", "", 10);
        userDataService.createExercice("Pompes", "pompes", "", 10);
        userDataService.createExercice("Pompes pieds surélevés", "pompes_pieds_surleves", "", 10);

        //Quadriceps
        userDataService.createExercice("Extension de jambes", "extension_de_jambes", "", 11);
        userDataService.createExercice("Fentes", "fentes", "", 11);
        userDataService.createExercice("Fentes marchées", "fentes_marchees", "", 11);
        userDataService.createExercice("Front squat barre", "front_squat_barre", "", 11);
        userDataService.createExercice("Genoux levés / courir sur place", "genoux_leves_courir_sur_place", "", 11);
        userDataService.createExercice("Presse à cuisses", "presse_a_cuisses", "", 11);//90
        userDataService.createExercice("Sauts sur place", "sauts_sur_place", "", 11);
        userDataService.createExercice("Split squat", "split_squat", "", 11);
        userDataService.createExercice("Squat barre", "squat_barre", "", 11);
        userDataService.createExercice("Squat sumo", "squat_sumo", "", 11);
        userDataService.createExercice("Squat sur banc à la barre", "squat_sur_banc_avec_halteres", "", 11);
        userDataService.createExercice("Squats", "squats", "", 11);
        userDataService.createExercice("Steps avec chaise", "steps_sur_chaise", "", 11);

        //Trapèzes
        userDataService.createExercice("Shrug à la Smith machine", "shrug_a_la_smith_machine", "", 12);
        userDataService.createExercice("Shrug arrière à la barre", "shrug_arriere_a_la_barre", "", 12);
        userDataService.createExercice("Shrug arrière à la Smith machine", "shrug_arriere_a_la_smith_machine", "", 12);//100
        userDataService.createExercice("Shrug avec haltères", "shrug_avec_haltere", "", 12);
        userDataService.createExercice("Shrug devant à la barre", "shrug_devant_a_la_barre", "", 12);

        //Triceps
        userDataService.createExercice("Barre front avec barre", "barre_front_avec_barre", "", 13);
        userDataService.createExercice("Développé couché prise serrée", "developpe_couche_prise_serree", "", 13);
        userDataService.createExercice("Dips", "dips", "", 13);
        userDataService.createExercice("Extension triceps à la poulie haute (corde)", "extension_triceps_poulie_corde", "", 13);
        userDataService.createExercice("Extension triceps à la poulie haute (barre)", "extension_triceps_poulie_barre", "", 13);
        userDataService.createExercice("Extension triceps aux dessus de la tête assis", "extension_triceps_au_dessus_de_la_tete", "", 13);
        userDataService.createExercice("Extension triceps un bras à la poulie", "extension_triceps_un_bras_poulie", "", 13);
        userDataService.createExercice("Kickback unilatéral avec haltère", "kickback_unilateral_avec_haltere", "", 13);//110
        userDataService.createExercice("Pompes diamant", "pompes_diament", "", 13);
        userDataService.createExercice("Pompes mains serrées", "pompes_mains_serrees", "", 13);
        userDataService.createExercice("Triceps sur chaise", "triceps_sur_chaise", "", 13);//113

        userDataService.close();
    }

    /**
     * Fonction permettant de créer les séances prédéfinis
     */
    private void createSeancesPredefinis(){
        userDataService = new UserDataService(LandingActivity.this);
        userDataService.open();

        userDataService.createSeance("Séance abdos", "Sans matériel", 1);
        userDataService.addExerciceToSeance(1, 1, 15, 3, 30);
        userDataService.addExerciceToSeance(5, 1, 60, 3, 30);
        userDataService.addExerciceToSeance(6, 1, 60, 2, 60);
        userDataService.addExerciceToSeance(8, 1, 15, 3, 30);

        userDataService.createSeance("Séance bras", "2 haltères", 1);
        userDataService.addExerciceToSeance(11, 2, 10, 3, 30);
        userDataService.addExerciceToSeance(19, 2, 10, 3, 30);
        userDataService.addExerciceToSeance(24, 2, 10, 3, 30);

        userDataService.createSeance("Séance dos", "Poids du corps avec une barre de traction en Superset", 1);
        userDataService.addExerciceToSeance(23, 3, 5, 3, 15);
        userDataService.addExerciceToSeance(24, 3, 10, 3, 60);
        userDataService.addExerciceToSeance(23, 3, 12, 3, 15);
        userDataService.addExerciceToSeance(24, 3, 10, 3, 60);

        userDataService.createSeance("Séance épaules", "2 haltères", 1);
        userDataService.addExerciceToSeance(40, 4, 12, 3, 30);
        userDataService.addExerciceToSeance(42, 4, 12, 3, 30);
        userDataService.addExerciceToSeance(43, 4, 12, 3, 30);
        userDataService.addExerciceToSeance(36, 4, 12, 3, 30);

        userDataService.createSeance("Séance jambes", "Sans matériel", 1);
        userDataService.addExerciceToSeance(96, 5, 15, 3, 30);
        userDataService.addExerciceToSeance(89, 5, 15, 3, 30);
        userDataService.addExerciceToSeance(54, 5, 20, 3, 30);
        userDataService.addExerciceToSeance(67, 5, 20, 3, 30);
        userDataService.addExerciceToSeance(68, 5, 20, 3, 30);

        userDataService.createSeance("Séance pectoraux", "2 haltères", 1);
        userDataService.addExerciceToSeance(78, 6, 15, 3, 30);
        userDataService.addExerciceToSeance(79, 6, 10, 3, 30);
        userDataService.addExerciceToSeance(77, 6, 10, 3, 30);
        userDataService.addExerciceToSeance(106, 6, 10, 3, 30);
        userDataService.addExerciceToSeance(84, 6, 10, 3, 30);

        userDataService.close();
    }

    /**
     * Fonction permettant de créer les familles d'exercices dans la base de données
     */
    private void createFamilleRecettes(){
        userDataService = new UserDataService(LandingActivity.this);
        userDataService.open();

        userDataService.createFamilleRecettes("Petit-déjeuner");
        userDataService.createFamilleRecettes("Déjeuner");
        userDataService.createFamilleRecettes("Dîner");
        userDataService.createFamilleRecettes("Collations");

        userDataService.close();
    }

    private void createRecettes() {
        userDataService = new UserDataService(LandingActivity.this);
        userDataService.open();

        //Petit-déjeuner
        userDataService.createRecette("Pancake protéiné","pancakes_proteines","- 50g d'avoine instantanée (ou farine d'avoine)\n- 1 verre de lait\n- 1 oeuf entier\n- 20g de framboise en morceaux","Mélanger le tout dans un récipient.\nFaites cuire sur une poêle (1m30 de chaque côté).",1);
        userDataService.createRecette("Bol de flocons d'avoine","bol_flocons_davoine","- 100g de flocons d'avoine\n- 250ml de lait chocolaté","Verser le flocons d'avoine dans un bol.\nAjouter le lait chocolaté par-dessus.\n Peut se combiner avec une tartine de pain au beurre d'amande/de cacahuète protéiné.",1);
        userDataService.createRecette("Porridge protéiné au chocolat","porridge_chocolat","- 60 g de flocons d’avoine\n- 1 dosette de whey au chocolat\n- 100 ml de lait d’amande","Mélanger dans un bol.\nFaire chauffer 30 secondes environ au micro-onde.",1);
        userDataService.createRecette("Omelette aux petit-légumes","omelette_legumes","- 1 œuf entier\n- 100g de blanc d'oeuf\n- 1/4 tomate\n- 1/4 poivrons","Faire revenir dans une poêle anti-adhésive des champignons, des tomates, des oignons, de l’ail etc. (légumes au choix). Une fois dorés, ajouter 3 œufs entiers et faire cuire.",1);
        userDataService.createRecette("crêpes protéinées","crepes_proteinees","- 60 g de flocons d'avoine en poudre( Si vous n’avez pas d’avoine en poudre, il suffit de les faire passer quelques instants au blender)\n- 25 g de protéine de blanc d’œuf\n- 00 ml d'eau (ou de lait d'avoine, d'amande, de soja)","On mélange tout et on fait cuire à la poêle.",1);
        userDataService.createRecette("Muesli gourmand au lait d'avoine","muesli_gourmand","- 60 g de muesli bio nature\n- 1 à 2 cuillères à soupe de sucre en poudre\n- Quelques cubes de mangues ou de fruis frais\n- 1 poignée de fruits secs","- Mélangez le lait et le sucre dans un grand bos\n- Ajouter le muesli, les fruits frais et les fruits secs",1);
        userDataService.createRecette("Banana bread aux flocons d'avoine","banana_bread","- 2 bananes bien mûres\n- 250 g de farine\n- 85g de fromage blanc 0%\n- 6 cuillières a soupes de flocons d'avoines\n- 2 cuillière a soupe de levure chimique\n- 1 pincée de sel","Préchauffez le four à 170°C.\nPelez et écrasez les bananes dans un saladier.\nAjoutez les oeufs, le fromage blanc, l'édulcorant et le lait puis mélangez bien.\nIncorporez la farine, le sel et la levure chimique, en remuant bien.Versez la pâte dans un moule en silicone.\nRépartissez sur le dessus les flocons d'avoine.Enfournez pendant 50 minutes environ.\nServez tiède ou froid.",1);
        userDataService.createRecette("Croissant brioché diététique au yaourt 0%","croissant_brioches","500 g de farine.\n- 15 cl de lait écrémé tiède1 oeuf.\n- 60 g de yaourt nature 0%.\n- 60 g de margarine végétale.\n- 1 cuillère à soupe de levure de boulanger déshydraté.\n- 1/2 cuillère à café de levure chimique.\n- 1 pincée de sel","Mélangez la farine, les levures et le sel dans un récipient.\n Ajoutez le lait, l'oeuf, le yaourt et la margarine puis pétrissez à la main jusqu'à obtenir une boule de pâte souple non collante.\n Couvrez d'un torchon propre et laissez lever pendant 1h30 à température ambiante.\n Dégazez ensuite la pâte sur un plan de travail fariné et étalez-la en un long rectangle à l'aide d'un rouleau à pâtisserie.\n Découpez des triangles et roulez-les (en partant du côté le plus large) pour former les croissants.\n Déposez-les sur une plaque de four recouverte de papier sulfurisé.\n Couvrez la plaque d'un torchon propre et laissez lever pendant 45 minutes.\n Préchauffez le four à 200°C.\n Badigeonnez les croissants avec un peu de lait, à l'aide d'un pinceau.\n Enfournez pendant 15 minutes.\n Laissez tiédir avant de décoller les croissants.",1);

        //Déjeuner
        userDataService.createRecette("Poulet à l’orientale","poulet_a_loriental","500 g d’escalope de poulet.\n- 1 gros oignon émince\n- 45 cl de bouillon de poule dégraissé\n- 1 poivron vert épépiné coupé en dés\n- 800 g de tomate fraîche émincée\n- 450 g de pois chiches en boîte\n- 1 grande feuille de laurier\n- 1 cuillère à café de feuille de thym séché\n- 1 cuillère à café de cumin en poudre\n- 1/4 de cuillère à café de poudre 4 épices\n- Une pincée de poudre de clou de girofle\n- Une pincée de poivre noir moulu\n- 500 g de semoule à couscous non cuite","Découpez le poulet en 6 ou 7 gros morceaux et faite cuire à feu moyen en le retournant régulièrement, jusqu’à ce qu’il commence à dorer.\n- Retirez le poulet avec une écumoire et réservez-le dans un saladier. Dans le même plat en terre, ajoutez l’oignon, l’ail et 2 cuillerées à soupe de bouillon.\n- Remuez bien pour décoller tous les sucs du fond du plat.\n- Laissez cuire à feu moyen pendant 5 à 6 minutes, en remuant régulièrement ou jusqu’à ce que l’oignon soit tendre.\n- Si le liquide commence à s’évaporer, ajoutez plus de bouillon.\n- Remettez la viande dans le plat.\n- Ajoutez le reste du bouillon et les 10 autres ingrédients.\n- Portez à ébullition, diminuez le feu, couvrez, et laissez mijoter 20 à 25 minutes ou jusqu’à ce que le poulet soit tendre.\n- Retirez la feuille de laurier et jetez-la.\n- Tandis que le poulet mijote, préparez la semoule en suivant les instructions sur le paquet.\n- Disposez le semoule dans un plat et couvrez avec le poulet et la sauce.\n- Pour 6 personnes ajuster la quantité pour des portions d'une personne.\n- Bon appétit !",2);
        userDataService.createRecette("Poulet au parmesan","poulet_au_parmesan","escalopes de poulet\n- 50 ml d’une sauce tomate allégée de préférence\n- 1.5 cuillères à soupe de parmesan râpé","Préchauffer le four à 180 °C, puis préchauffer le grill à température maximum.\n Faites griller le poulet entre 3 et 5 minutes de chaque côté ou jusqu’à ce qu’il ne soit plus rosé à l’intérieur.\n Pendant ce temps, faites chauffer la sauce au micro-ondes.\n Placez le poulet grillé dans un plat allant au four, nappez ensuite chaque escalope de sauce tomate, puis saupoudrez de parmesan.\n Laissez cuire 3 à 5 minutes ou jusqu’à ce que le fromage soit fondu.\n Servez le poulet chaud, accompagné de pâtes et/ou d’une salade verte.",2);
        userDataService.createRecette("Salade de pâtes au saumon","salade_pate_saumon","50 g de saumon\n- 250 g de pâtes\n -50 g de brocoli frais\n -30 ml de lait écrémé\n -2 cuillères à soupe de mayonnaise allégée\n- 1 banane","Rassemblez tous les ingrédients (sauf la banane) dans un saladier et mélangez.\n La salade n’a pas besoin d’être glacées, mais si vous la placez au réfrigérateur pendant 30 minutes, les saveurs ne seront que meilleures.\n Dégustez cette salade froide accompagnée de la banane.",2);
        userDataService.createRecette("Poulet à la grecque","poulet_a_la_grecque","30 g de feta allégé en matière grasse\n- 25 g d’épinards hachés crus\n- 1 blancs de poulet sans peau","Préchauffer votre four à 180°.\n Dans un grand saladier, mélangez la feta et les épinards.\n Faites une petite entaille dans la partie la plus charnue de chaque blanc de poulet, puis agrandissez-la à l’aide du couteau pour en faire une petite poche en veillant à ne pas couper les côtés.\n Avec une cuillères, remplissez chaque poche du mélange fromage/épinards, puis placez les blancs dans un plat et faites les cuire au four entre 10 à 15 minutes selon votre goût.",2);
        userDataService.createRecette("Saumon poché au Pesto","saumon_poche_pesto","1 filets de saumon, sans la peau (175 g)\n -Eau\n -1 cuillère à soupe de sel\n- 40 g de Pesto prêt à l’emploi","Placez le saumon dans un faitout.\n Versez assez d’eau dans le plat pour couvrir les filets.\n Ajoutez du sel.\n Portez à ébullition, puis couvrez et laissez mijoter jusqu’à ce que le poisson soit opaque, soit environ 10 minutes.\n Dans un bol, pendant ce temps, ajoutez 64 ml d’eau au Pesto et mélangez pour lier la sauce.\n Retirez le saumon de la casserole avec une écumoire.\n Saupoudrez chaque filet avec le Pesto. C’est prêt ",2);
        userDataService.createRecette("Recette protéinée au thon","recette_proteine_thon","1 boîtes de 180 g de thon albacore au naturel\n- mayonnaise allégée (plus ou moins selon votre goût)\n- 50 g de cèleri coupé en dés\n- 50 g de légumes au vinaigre coupés en dés\n- 1/2 cuillère à café de poivre noir\n- 2 blancs d’oeuf dur hachés\n- 2 tranches de pain comple","Mélangez les six ingrédients et servez sur du pain grillé",2);
        userDataService.createRecette("Riz cantonais","riz_cantonais","75 g de blanc de poulet\n- 50 g de crevettes roses décortiquées\n- 1 oeuf\n- 15 g de petit pois\n- 150 g de riz\n- sel, poivre","Faites cuire une escalope de poulet et coupez la en petits morceaux d’un demi centimètre.\n Vous pouvez utilisez du blanc de poulet en jambon si vous êtes pressé. Coupez les crevettes en petits bout également. Faites cuire dans une poêle l'oeuf en omelette et coupez la en tout petits morceaux. Réservez.\n Dans un wok, faire chauffer l’huile d’olive jusqu’à ce qu’elle soit très chaude.\n Verser le riz et faites le sauter à feu très vif pendant 3 minutes sans cesser de remuer: il ne doit pas brûler.\n Ajoutez le poulet, les crevettes et les petits pois (avec sel et poivre).\n Laisser cuire encore 2 minutes, toujours à feu très vif en remuant.\n Vous pouvez ajouter de la ciboulette hachée et du bouillon de volaille selon votre goût.",2);
        userDataService.createRecette("Salade de pâtes au thon","salade_pate_thon","150 g de pâtes cuites et refroidies\n- 1 boîtes (150 g) de thon « blanc » au naturel, égoutté\n- 1 tomates moyennes en tranches\n- 30 g de coeurs d’artichaut hachés et égouttés\n- 30 g de concombre épluché et haché\n- 30 g de poivron jaune haché\n- 30 g de brocoli cuit à la vapeur égouttés\n- quelques olives selon votre goût\n Sauce :\n- 25 ml de vinaigre de vin\n- 1/2 cuillère à soupe d’huile d’olive\n- 1/2 cuillère à café de parmesan râpé\n- 1/2 cuillère à café d’ail émincé\n- 1/2 cuillère à café de basilic séché\n- 1/2 cuillère à café d’origan séché\n- sel et poivre selon votre goût","Mélangez les sept premiers ingrédients dans un grand récipient. Dans un autre récipient, préparez le mélange pour la sauce. Versez la sauce sur le mélange thon et pâtes et laissez le tout au réfrigérateur jusqu’au moment de servir.",2);

        //Dîner
        userDataService.createRecette("Brochettes de poulet","brochette_poulet","1/4 boîte de lait de coco\n- 1/2 cuillère à café de maïzena\n- 1/2 piment jalapeno, épépiné et émincé\n- 1/2 gousse d’ail écrasée\n- 1/2 cuillères à soupe d’échalotes hachées\n- 2 cuillères à soupe de beurre de cacahuète\n- le jus d’un citron vert\n- 1 blanc de poulet, sans peau et désossés, coupés en morceaux\n- coriandre hachée","Dans une petite casserole, mélangez le lait de coco et la maïzena.\n Ajoutez le piment, l’ail et l’échalote.\n Placez sur feu doux et portez à ébullition en remuant sans cesse.\n Laissez cuire 3 à 4 minutes, puis ajoutez le beurre de cacahuète et le jus de citron vert.\n Remuez jusqu’à ce que le beurre de cacahuète soit bien mélangé.\n Embrochez les morceaux de poulet sur les piques en bambou.\n Placez sur les brochettes sur le barbecue et faites cuire à coeur sur chaque face, pendant 4 à 5 minutes environs.\n Rajoutez une pincée de coriandre à la fin de la cuisson… C’est prêt !",3);
        userDataService.createRecette("Sandwich poulet Tabasco","sandwich_poulet_tabasco","280 g de blanc poulet\n- 1/2 cuillère à soupe d’oignon rouge\n- 1/2 cuillère à soupe de cèleri haché\n- salade et tomate selon votre goût\n- 1 cuillères à soupe de mayonnaise allégée\n- 1/2 cuillère à café de moutarde\n- une pincée de sel avec de l’ail moulu\n- quelques gouttes de Tabasco","Placez et découpez le poulet, l’oignon et le cèleri dans un saladier.\n Incorporez-y le mélange de mayonnaise, moutarde, sel/ail, tomate/salade et de Tabasco suivant votre convenance.",3);
        userDataService.createRecette("Poulet et polenta","poulet_et_polenta","- 2 cuisses de poulet avec la peau\n- 20 ml de tasse de bouillon de poule allégé en matière grasse et en sel\n- 1/2 tasses de sauces tomate\n- 50g g de légumes émincés (champignon, courgettes, oignons, etc)\n- 1 cuillère à soupe de basilic frais haché\n- 1 cuillère à café d’huile d'olive.\n- 100 g de polenta déjà prête","Enlevez le gras visible des cuisses de poulet, mais sans retirer la peau.\n Faites chauffer le bouillon de poule dans une poêle, ajoutez le poulet et faites cuire 5 minutes sur chaque face jusqu’à ce qu’il soit doré. Ajoutez la sauce tomate, les légumes, le basilic, l’ail et l’huile d’olive, si vous le souhaitez; laissez cuire à petit feu environ 15 minutes ou jusqu’à ce qu’une fourchette s’enfonce aisément.\n Pendant la cuisson, découpez la polenta en tranches de 1,5 cm environ et faites réchauffer 2 à 3 minutes de chaque côté dans une poêle.\n Disposez la polenta sur l’assiette et couvrez avec le poulet et la sauce tomate.\n Ôtez la peau du poulet avant de le manger.",3);
        userDataService.createRecette("Poulet au four","poulet_au_four","1 blanc de poulet (environ 170 g)\n- 3 à 4 cuillères à soupe de sauce pour grillade (ou toute autre sauce prête à utiliser)","Laisser mariner le poulet dans la sauce pendant une heure.\n Pour rehausser le goût, laisser mariner toute la nuit au réfrigérateur.\n Préchauffez le gril pendant 5 minutes.\n Placez le poulet mariné dans son récipient à environ 13 cm sous le grill et faites griller pendant sept minutes environ (laissez la porte légèrement ouverte pour un four électrique).\n Retournez le poulet et faites griller pendant encore sept minutes.\n Retirez le plat du four et vérifiez la cuisson à l’aide d’un thermomètre à viande ; le blanc de poulet devra être à 75 °C.\n À défaut de thermomètre, coupez le morceau de blanc au centre : si la chair est encore rose, prolongez la cuisson de cinq minutes ou jusqu’à ce que le centre soit tout à fait blanc (le temps de cuisson est fonction de l’épaisseur du morceau).",3);
        userDataService.createRecette("Escalope de poulet farcies à la mozzarella","escalope_poulet_farcis_mozarella","1 escalope de poulet (environ 500 g au total)\n- 25 g de riz complet cuit (dans un bouillon de poule)\n- Une petite tomate fraîche coupée en dés\n- 10 g de mozzarella émiettée\n- Selon votre goût : basilic haché, huile d’olive, sel, poivre moulu","Assaisonnez d’un seul côté les escalopes de poulet avec le sel et la moitié du poivre préparé.\n Dans un petit saladier, mélangez le riz, les tomates, le fromage, le basilic et le reste du poivre.\n Répartissez le mélange à base de riz sur les escalopes, repliez-les et maintenez-les fermées à l’aide de cure-dents.\n Épongez l’extérieur des escalopes avec du papier absorbant. Faites chauffer l’huile dans une grande poêle à feu moyen. Faites cuire les escalopes dans l’huile chaude une minute de chaque côté, ou jusqu’à ce qu’elles soient dorées, puis placez-les dans un plat peu profond allant au four et faites cuire à 190°C pendant 8 à 10 minutes.\n Laissez reposer 5 minutes avant de découper.",3);
        userDataService.createRecette("Thon nappé à la moutarde","thon_moutarde","- 30 g de moutarde\n- 1/2 cuillères à soupe de sauce soja\n- 1/2 cuillères à soupe de jus de citron vert\n- 1 filets de thon (175 g)","Préchauffez le four à 190 °C / Thermostat 5.\n Mélangez la moutarde, la sauce soja et le jus de citron vert dans un petit bol.\n À l’aide d’un pinceau, enduisez chaque côté du filet de thon et réservez le reste de la sauce.\n Déposez le filet de thon et réservez le reste de la sauce.\n Déposez le filet sur un plat de cuisson.\n Faites cuire au four pendant 15 minutes ou jusqu’à cuisson complète.",3);
        userDataService.createRecette("Saumon épicé","saumon_epice","1/4 oignon rouge émincé\n-1/2 gousses d’ail écrasées\n- 1/4 cuillères à café d’une sauce pimentée au choix\n- 1/4 cuillère à soupe de coriandre\n- 1 tomates cerise coupées en 4\n- 15 g de feuille d’épinards\n- 1 filets de saumon (115 g)\n- Sel et poivre selon votre goût\n- Citron vert","Préchauffez votre four à 200 °C/th.6.\n Garnissez une plaque de cuisson à l’aide de papier aluminium.\n Mélanger l’oignon, l’ail, les piments, la coriandre et les tomates dans un saladier.",3);
        userDataService.createRecette("Salade de haricots","salade_haricots","- 75 g de haricots verts entiers, égouttés\n- 120 g de pois chiches, égouttés\n- 70 g de haricots rouges, égouttés\n- 50 g de maïs doux en grains égouttés\n- 1/4 poivron émincé\n- Sauce salade à 0 %\n- 15 g de gruyère ou râpé extra allégé ou 0 %\n- 1 grandes feuilles de laitue","Mélangez les haricots, le maïs et les oignons dans un grand saladier.\n Laissez une heure (ou toute la nuit) au réfrigérateur.\n Avant de servir, incorporez le fromage puis déposez la salade sur les feuilles de laitue, dans des bols ou des assiettes.",3);

        //Collation
        userDataService.createRecette("Shaker de Whey","shaker_whey","- 1 scoop de protéine Whey (peu importe la marque\n- 250ml d'eau ou de lait","Mélanger la Whey et l'eau ou le lait.\n Remuer dans un shaker.\n Boire instantanément",4);
        userDataService.createRecette("Banane","banane","- Banane de 100g","Riche en glucides, la banane est un excellent fruit en collation à manger cru.",4);
        userDataService.createRecette("Pumpkin Pie Protein Shake","pumpkin_protein_shake","- Protéines Whey en poudre, \n- Beurre de cacahuète allégé en graisses, \n- Yaourt grec à 0%, \n- Boite de potiron en purée, \n- Lait d’amandes aromatisé à la vanille, \n- Edulcorant type stevia","Mélanger le tout dans un bol en pesant soigneusement les ingrédient. Ensuite, passer au blender pour un mélange encore plus onctueux, avec un peu de glaçons et de canelle. Servir enfin dans de grands verres transparents et recouvrir de crème chantilly allégée. \nApport calorique: 497 calories \nProtéines: 56g,\nGlucides: 48g,\nLipides: 9g.",4);
        userDataService.createRecette("Cookie protéiné","cookies_chocolat_proteines","- 50 g de sucre de coco\n- 150 g de beurre d’amandes \n- 1 oeuf\n- 1 cuillère à café de levure chimique\n- 35 g de cacao en poudre non sucré\n- 25 g de pépites de chocolat noir\n- 1 cuillère à soupe de fécule de pommes de terre","Préchauffer le four à 180°C.\nMélanger le sucre de coco (ou cassonade) avec la purée d’amandes ou du beurre de cacahuète.Ajouter l’oeuf battu, la levure, le cacao et les pépites de chocolat. Mélanger avec une cuillère en bois jusqu’à ce que la pâte commence à s’amalgamer.\nAjouter la fécule de pommes de terre et former une boule de pâte avec les mains.\nSéparer la pâte en dix portions égales. Bouler chacune des portions.\nDéposer les boules sur une plaque de cuisson anti-adhérente (ou recouverte de papier sulfurisé). Les aplatir.\nEnfourner 10 minutes.\nA la sortie du four, les cookies sont encore un peu mous mais vont durcir en refroidissant.",4);
    }

    @Override
    public void onLoadDataComplete() {
        startActivity(new Intent(LandingActivity.this, AccueilActivity.class));
        finish();
    }

    public class LoadData extends AsyncTask {

        private ProgressDialog progressDialog;
        private LoadDataComplete loadDataComplete;

        public LoadData(LoadDataComplete loadDataComplete) {
            this.loadDataComplete = loadDataComplete;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LandingActivity.this);
            progressDialog.setMessage("Création du profil...");
            progressDialog.setTitle("Chargement");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(Object[] params) {

            //Création des familles d'exercices
            createFamilleExerices();
            //Création des exercices
            createExercices();
            //Création des séances prédéfinis
            createSeancesPredefinis();
            //Création des familles de recettes
            createFamilleRecettes();
            //Création des recettes
            createRecettes();
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progressDialog.dismiss();
            loadDataComplete.onLoadDataComplete();
        }
    }




}
