package com.example.philippe.moveandeat.Object;

public class Exercices {

    private int id, id_famille;
    private String libelle, path_image, description;

    public Exercices() {
    }

    public Exercices(int id, int id_famille, String libelle, String path_image, String description) {
        this.id = id;
        this.id_famille = id_famille;
        this.libelle = libelle;
        this.path_image = path_image;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_famille() {
        return id_famille;
    }

    public void setId_famille(int id_famille) {
        this.id_famille = id_famille;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
