package com.example.philippe.moveandeat.Object;


public class ExericeSeance {

    private int id, id_famille, nb_repetition, nb_serie, temps_repos;
    private String libelle, path_image, description;

    public ExericeSeance() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_famille() {
        return id_famille;
    }

    public void setId_famille(int id_famille) {
        this.id_famille = id_famille;
    }

    public int getNb_repetition() {
        return nb_repetition;
    }

    public void setNb_repetition(int nb_repetition) {
        this.nb_repetition = nb_repetition;
    }

    public int getNb_serie() {
        return nb_serie;
    }

    public void setNb_serie(int nb_serie) {
        this.nb_serie = nb_serie;
    }

    public int getTemps_repos() {
        return temps_repos;
    }

    public void setTemps_repos(int temps_repos) {
        this.temps_repos = temps_repos;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
