package com.example.philippe.moveandeat.Object;

/**
 * Created by Damian on 17/06/2017.
 */

public class FamilleRecettes {

    private int id;
    private String libelle;

    public FamilleRecettes() {
    }

    public FamilleRecettes(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
