package com.example.philippe.moveandeat.Object;

/**
 * Created by Damian on 17/06/2017.
 */

public class Recettes {

    private int id, id_famille;
    private String libelle, path_image, ingredients, preparation;

    public Recettes() {
    }

    public Recettes(int id, int id_famille, String libelle, String path_image, String ingredients, String preparation) {
        this.id = id;
        this.id_famille = id_famille;
        this.libelle = libelle;
        this.path_image = path_image;
        this.ingredients = ingredients;
        this.preparation = preparation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_famille() {
        return id_famille;
    }

    public void setId_famille(int id_famille) {
        this.id_famille = id_famille;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getPath_image() {
        return path_image;
    }

    public void setPath_image(String path_image) {
        this.path_image = path_image;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }
}
