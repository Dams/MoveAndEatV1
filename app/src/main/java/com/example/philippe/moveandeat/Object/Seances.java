package com.example.philippe.moveandeat.Object;

public class Seances {

    private int id, is_predef;
    private String libelle, description;

    public Seances() {
    }

    public Seances(int id, int is_predef, String libelle, String description) {
        this.id = id;
        this.is_predef = is_predef;
        this.libelle = libelle;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIs_predef() {
        return is_predef;
    }

    public void setIs_predef(int is_predef) {
        this.is_predef = is_predef;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
