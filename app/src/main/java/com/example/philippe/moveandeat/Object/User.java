package com.example.philippe.moveandeat.Object;


public class User {

    private int idUser;
    private String prenomU, date_inscription;
    private int poidsU, taille, objectif, sexe, xp, level;

    /**
     * Constructeur vide
     */
    public User() {
    }

    /**
     * Constructeur de la classe User
     * @param prenomU
     * @param poidsU
     * @param taille
     * @param objectif
     * @param date_inscription
     */
    public User( String prenomU, int sexe, int poidsU, int taille, int objectif, String date_inscription, int xp, int level) {
        this.prenomU = prenomU;
        this.sexe = sexe;
        this.poidsU = poidsU;
        this.taille = taille;
        this.objectif = objectif;
        this.date_inscription = date_inscription;
        this.xp = xp;
        this.level = level;
    }


    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getPrenomU() {
        return prenomU;
    }

    public void setPrenomU(String prenomU) {
        this.prenomU = prenomU;
    }

    public String getDate_inscription() {
        return date_inscription;
    }

    public void setDate_inscription(String date_inscription) {
        this.date_inscription = date_inscription;
    }

    public int getPoidsU() {
        return poidsU;
    }

    public void setPoidsU(int poidsU) {
        this.poidsU = poidsU;
    }

    public int getTaille() {
        return taille;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

    public int getObjectif() {
        return objectif;
    }

    public void setObjectif(int objectif) {
        this.objectif = objectif;
    }

    public int getSexe() {
        return sexe;
    }

    public void setSexe(int sexe) {
        this.sexe = sexe;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
