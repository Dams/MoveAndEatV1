package com.example.philippe.moveandeat.PagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.philippe.moveandeat.Fragments.AllExercicesFragment;
import com.example.philippe.moveandeat.Fragments.MesSeancesFragment;

public class PagerAdapter_Exercices extends FragmentStatePagerAdapter {

    /**
     * Description générale de la classe :
     *
     *      Cette classe permet de
     */

    int mNumOfTabs;

    /**
     * Constructeur de la classe PagerAdapter_Exercices
     * @param fm
     * @param mNumOfTabs
     */
    public PagerAdapter_Exercices(FragmentManager fm, int mNumOfTabs) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                MesSeancesFragment tab1 = new MesSeancesFragment();
                return tab1;
            case 1:
                AllExercicesFragment tab2 = new AllExercicesFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
