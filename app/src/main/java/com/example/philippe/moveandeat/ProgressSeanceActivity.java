package com.example.philippe.moveandeat;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Object.ExericeSeance;
import com.example.philippe.moveandeat.Object.User;
import com.example.philippe.moveandeat.Utils.Utils;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ProgressSeanceActivity extends AppCompatActivity {

    private ListView List_ProgressSeance;
    private int id_seance, nb_exercices;
    private List<ExericeSeance> list = new ArrayList<ExericeSeance>();
    private List<String> list_checked = new ArrayList<String>();
    private UserDataService userDataService;

    private SwipeFlingAdapterView flingContainer;
    private List<String> al = new ArrayList<String>();
    private ArrayAdapter arrayAdapter;
    private Adapter_ListExercicesProgressSeance adapter_listExercicesProgressSeance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_seance);

        Intent intent = getIntent();
        id_seance = intent.getIntExtra("id", 0);

        flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);

        //Récupération des exercices de la séance
        userDataService = new UserDataService(ProgressSeanceActivity.this);
        userDataService.open();
        list = userDataService.getExercicesInSeance(id_seance);
        nb_exercices = list.size();
        userDataService.close();

        adapter_listExercicesProgressSeance = new Adapter_ListExercicesProgressSeance(
                list,
                ProgressSeanceActivity.this);


        flingContainer.setAdapter(adapter_listExercicesProgressSeance);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                list.remove(0);
                adapter_listExercicesProgressSeance.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object o) {
                checkIsFinish();
            }

            @Override
            public void onRightCardExit(Object o) {
                checkIsFinish();
            }

            @Override
            public void onAdapterAboutToEmpty(int i) {

            }

            @Override
            public void onScroll(float v) {

            }
        });
    }

    /**
     * Permet de checker si la séance est finis
     */
    private void checkIsFinish(){
        //Vérification qu'il ne reste plus d'exo
        if(list.size() == 0){
            //La séance est terminer, on ajoute l'XP à l'utilisateur
            int xp = nb_exercices * 100; //Un exercice = 100 d'xp

            //Récupération de l'xp de l'utilisateur
            userDataService = new UserDataService(ProgressSeanceActivity.this);
            userDataService.open();

            User user = userDataService.getUser();
            xp = xp + user.getXp();

            //Update de l'xp
            userDataService.updateUserXP(xp);
            userDataService.close();

            //Vérification que l'utilisateur à Up ou non
            int level_of_user = user.getLevel();
            int level_current = Utils.getLevelOfUser(xp);

            if(level_of_user != level_current){
                userDataService = new UserDataService(ProgressSeanceActivity.this);
                userDataService.open();
                userDataService.updateUserLevel(level_current);
                userDataService.close();

                new SweetAlertDialog(ProgressSeanceActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Bravo !")
                        .setContentText("Vous avez atteint le niveau " + level_current)
                        .setConfirmText("Fermer")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                finish();
                            }
                        })
                        .show();
            }else{
                finish();
            }
        }
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ProgressSeanceActivity.this);
        builder.setTitle("Attention");
        builder.setMessage("Voulez-vous vraiment quitter cette séance ? Vos données ne seront pas sauvegardées.");
        builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private class Adapter_ListExercicesProgressSeance extends BaseAdapter {

        private List<ExericeSeance> list_exercices;
        private Activity activity;

        public Adapter_ListExercicesProgressSeance(List<ExericeSeance> _list_exercices, Activity activity) {
            this.list_exercices = _list_exercices;
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return list_exercices.size();
        }

        @Override
        public Object getItem(int position) {
            return this.list_exercices.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {

            if(convertView == null){
                LayoutInflater layoutInflater = (LayoutInflater) activity.getApplicationContext().getSystemService(activity.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.adapter_list_exercices_progress_seance, parent, false);
            }

            TextView TXT_AdapterListExerciceProgressSeance_Name = (TextView) convertView.findViewById(R.id.TXT_AdapterListExerciceProgressSeance_Name);
            TextView TXT_AdapterListExerciceProgressSeance_Series = (TextView) convertView.findViewById(R.id.TXT_AdapterListExerciceProgressSeance_Series);
            TextView TXT_AdapterListExerciceProgressSeance_Reps = (TextView) convertView.findViewById(R.id.TXT_AdapterListExerciceProgressSeance_Reps);
            TextView TXT_AdapterListExerciceProgressSeance_Repos = (TextView) convertView.findViewById(R.id.TXT_AdapterListExerciceProgressSeance_Repos);
            ImageView IMG_AdapterListExerciceProgressSeance = (ImageView) convertView.findViewById(R.id.IMG_AdapterListExerciceProgressSeance);

            TXT_AdapterListExerciceProgressSeance_Name.setText(list_exercices.get(position).getLibelle());
            TXT_AdapterListExerciceProgressSeance_Series.setText(list_exercices.get(position).getNb_serie() + "");
            TXT_AdapterListExerciceProgressSeance_Reps.setText(list_exercices.get(position).getNb_repetition() + "");
            TXT_AdapterListExerciceProgressSeance_Repos.setText(list_exercices.get(position).getTemps_repos() + "''");
            return convertView;
        }
    }



}
