package com.example.philippe.moveandeat;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;

import java.util.ArrayList;

public class RecompensesActivity extends AppCompatActivity {

    private ImageView IMG_Recompenses_Level1, IMG_Recompenses_Level2, IMG_Recompenses_Level3, IMG_Recompenses_Level4,
            IMG_Recompenses_Level5, IMG_Recompenses_Level6, IMG_Recompenses_Level7, IMG_Recompenses_Level8, IMG_Recompenses_Level9,
            IMG_Recompenses_Level10, IMG_Recompenses_Level11, IMG_Recompenses_Level12, IMG_Recompenses_Level13, IMG_Recompenses_Level14,
            IMG_Recompenses_Level15, IMG_Recompenses_Level16, IMG_Recompenses_Level17, IMG_Recompenses_Level18, IMG_Recompenses_Level19,
            IMG_Recompenses_Level20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recompenses);

        IMG_Recompenses_Level1 = (ImageView) findViewById(R.id.IMG_Recompenses_Level1);
        IMG_Recompenses_Level2 = (ImageView) findViewById(R.id.IMG_Recompenses_Level2);
        IMG_Recompenses_Level3 = (ImageView) findViewById(R.id.IMG_Recompenses_Level3);
        IMG_Recompenses_Level4 = (ImageView) findViewById(R.id.IMG_Recompenses_Level4);
        IMG_Recompenses_Level5 = (ImageView) findViewById(R.id.IMG_Recompenses_Level5);
        IMG_Recompenses_Level6 = (ImageView) findViewById(R.id.IMG_Recompenses_Level6);
        IMG_Recompenses_Level7 = (ImageView) findViewById(R.id.IMG_Recompenses_Level7);
        IMG_Recompenses_Level8 = (ImageView) findViewById(R.id.IMG_Recompenses_Level8);
        IMG_Recompenses_Level9 = (ImageView) findViewById(R.id.IMG_Recompenses_Level9);
        IMG_Recompenses_Level10 = (ImageView) findViewById(R.id.IMG_Recompenses_Level10);
        IMG_Recompenses_Level11 = (ImageView) findViewById(R.id.IMG_Recompenses_Level11);
        IMG_Recompenses_Level12 = (ImageView) findViewById(R.id.IMG_Recompenses_Level12);
        IMG_Recompenses_Level13 = (ImageView) findViewById(R.id.IMG_Recompenses_Level13);
        IMG_Recompenses_Level14 = (ImageView) findViewById(R.id.IMG_Recompenses_Level14);
        IMG_Recompenses_Level15 = (ImageView) findViewById(R.id.IMG_Recompenses_Level15);
        IMG_Recompenses_Level16 = (ImageView) findViewById(R.id.IMG_Recompenses_Level16);
        IMG_Recompenses_Level17 = (ImageView) findViewById(R.id.IMG_Recompenses_Level17);
        IMG_Recompenses_Level18 = (ImageView) findViewById(R.id.IMG_Recompenses_Level18);
        IMG_Recompenses_Level19 = (ImageView) findViewById(R.id.IMG_Recompenses_Level19);
        IMG_Recompenses_Level20 = (ImageView) findViewById(R.id.IMG_Recompenses_Level20);

        ArrayList<ImageView> arrayList = new ArrayList<>();
        arrayList.add(IMG_Recompenses_Level1);
        arrayList.add(IMG_Recompenses_Level2);
        arrayList.add(IMG_Recompenses_Level3);
        arrayList.add(IMG_Recompenses_Level4);
        arrayList.add(IMG_Recompenses_Level5);
        arrayList.add(IMG_Recompenses_Level6);
        arrayList.add(IMG_Recompenses_Level7);
        arrayList.add(IMG_Recompenses_Level8);
        arrayList.add(IMG_Recompenses_Level9);
        arrayList.add(IMG_Recompenses_Level10);
        arrayList.add(IMG_Recompenses_Level11);
        arrayList.add(IMG_Recompenses_Level12);
        arrayList.add(IMG_Recompenses_Level13);
        arrayList.add(IMG_Recompenses_Level14);
        arrayList.add(IMG_Recompenses_Level15);
        arrayList.add(IMG_Recompenses_Level16);
        arrayList.add(IMG_Recompenses_Level17);
        arrayList.add(IMG_Recompenses_Level18);
        arrayList.add(IMG_Recompenses_Level19);
        arrayList.add(IMG_Recompenses_Level20);

        //Récupération du niveau de l'utilisateur
        UserDataService userDataService = new UserDataService(RecompensesActivity.this);
        userDataService.open();
        int level = userDataService.getUser().getLevel();
        userDataService.close();

        for (int i = 1; i <= level; i++){
            String uri = "@drawable/badge_" + i;
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            arrayList.get(i-1).setImageDrawable(res);
        }








    }
}
