package com.example.philippe.moveandeat;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.philippe.moveandeat.DataService.UserDataService;

public class UpdateExericeInSeanceActivity extends AppCompatActivity {

    private int id_seance, id_exercice, nb_serie, nb_reps, temps_repos;
    private EditText EditTXT_UpdateExerciceInSeance_Series, EditTXT_UpdateExerciceInSeance_Reps,
            EditTXT_UpdateExerciceInSeance_Temps;
    private Button BTN_UpdateExerciceInSeance;
    private Toolbar Toolbar_UpdateExerciceInSeance;
    private UserDataService userDataService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_exerice_in_seance);

        Intent intent = getIntent();
        id_seance = intent.getIntExtra("id_seance", 0);
        id_exercice = intent.getIntExtra("id_exercice", 0);
        nb_serie = intent.getIntExtra("nb_serie", 0);
        nb_reps = intent.getIntExtra("nb_reps", 0);
        temps_repos = intent.getIntExtra("temps_repos", 0);

        Toolbar_UpdateExerciceInSeance = (Toolbar) findViewById(R.id.Toolbar_UpdateExerciceInSeance);
        Toolbar_UpdateExerciceInSeance.setTitle("Modification exercice");
        Toolbar_UpdateExerciceInSeance.setTitleTextColor(Color.rgb(255, 255, 255));
        setSupportActionBar(Toolbar_UpdateExerciceInSeance);

        EditTXT_UpdateExerciceInSeance_Series = (EditText) findViewById(R.id.EditTXT_UpdateExerciceInSeance_Series);
        EditTXT_UpdateExerciceInSeance_Reps = (EditText) findViewById(R.id.EditTXT_UpdateExerciceInSeance_Reps);
        EditTXT_UpdateExerciceInSeance_Temps = (EditText) findViewById(R.id.EditTXT_UpdateExerciceInSeance_Temps);
        BTN_UpdateExerciceInSeance = (Button) findViewById(R.id.BTN_UpdateExerciceInSeance);

        EditTXT_UpdateExerciceInSeance_Series.setText(nb_serie+"");
        EditTXT_UpdateExerciceInSeance_Reps.setText(nb_reps+"");
        EditTXT_UpdateExerciceInSeance_Temps.setText(temps_repos+"");

        BTN_UpdateExerciceInSeance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userDataService = new UserDataService(UpdateExericeInSeanceActivity.this);
                userDataService.open();
                userDataService.updateOneExerciceInSeance(id_seance, id_exercice,
                        Integer.parseInt(EditTXT_UpdateExerciceInSeance_Series.getText().toString()),
                        Integer.parseInt(EditTXT_UpdateExerciceInSeance_Reps.getText().toString()),
                        Integer.parseInt(EditTXT_UpdateExerciceInSeance_Temps.getText().toString()));
                userDataService.close();
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
