package com.example.philippe.moveandeat;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Object.FamilleExercices;
import com.example.philippe.moveandeat.Object.User;

import java.util.ArrayList;
import java.util.List;

public class UpdateExervicesActivity extends AppCompatActivity {

    private Toolbar Toolbar_UpdateExercice;
    private EditText EditTXT_UpdateExercice;
    private Spinner SPIN_UpdateExercice;
    private Button BTN_UpdateExercice;
    private UserDataService userDataService;
    private List<FamilleExercices> list;
    private List<String> list_name_famille = new ArrayList<String>();
    private int id_famille_selected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_exervices);

        Intent intent = getIntent();
        final int id = intent.getIntExtra("id", 0);
        int id_famille = intent.getIntExtra("id_famille", 0);
        String libelle = intent.getStringExtra("libelle");

        Toolbar_UpdateExercice = (Toolbar) findViewById(R.id.Toolbar_UpdateExercice);
        EditTXT_UpdateExercice = (EditText) findViewById(R.id.EditTXT_UpdateExercice);
        SPIN_UpdateExercice = (Spinner) findViewById(R.id.SPIN_UpdateExercice);
        BTN_UpdateExercice = (Button) findViewById(R.id.BTN_UpdateExercice);
        Toolbar_UpdateExercice.setTitle("Détail de la recette");
        Toolbar_UpdateExercice.setTitleTextColor(Color.rgb(255, 255, 255));
        setSupportActionBar(Toolbar_UpdateExercice);

        EditTXT_UpdateExercice.setText(libelle);
        id_famille_selected = id_famille;

        //Préparation du Spinner
        userDataService = new UserDataService(UpdateExervicesActivity.this);
        userDataService.open();
        list = userDataService.getFamilleExercices();
        userDataService.close();

        for (int i = 0; i < list.size(); i++){
            list_name_famille.add(list.get(i).getLibelle());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list_name_famille);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPIN_UpdateExercice.setAdapter(dataAdapter);
        SPIN_UpdateExercice.setSelection(id_famille_selected - 1);

        SPIN_UpdateExercice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                id_famille_selected = list.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        BTN_UpdateExercice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!EditTXT_UpdateExercice.getText().toString().equals("")){
                    userDataService.open();
                    userDataService.updateExercice(id, EditTXT_UpdateExercice.getText().toString(), id_famille_selected);
                    userDataService.close();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Merci d'entrer le nom de l'exercice.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
