package com.example.philippe.moveandeat;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.philippe.moveandeat.DataService.UserDataService;
import com.example.philippe.moveandeat.Object.FamilleExercices;

import java.util.ArrayList;
import java.util.List;

import static com.example.philippe.moveandeat.R.id.IMG_Exercice;

public class UpdateRecettesActivity extends AppCompatActivity {

    private Toolbar Toolbar_DetailRecette;
    private TextView TxtView_NomRecette;
    private ImageView ImgView_Recette;
    private TextView TxtView_Ingredients;
    private TextView TxtView_Preparation;
    private Button BTN_FermerDetail;
    private UserDataService userDataService;
    private List<FamilleExercices> list;
    private List<String> list_name_famille = new ArrayList<String>();
    private int id_famille_selected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_recette);

        Intent intent = getIntent();
        final int id = intent.getIntExtra("id", 0);
        int id_famille = intent.getIntExtra("id_famille", 0);
        String libelle = intent.getStringExtra("libelle");
        String image = intent.getStringExtra("image");
        String ingredients = intent.getStringExtra("ingredient");
        String preparation = intent.getStringExtra("preparation");

        Toolbar_DetailRecette = (Toolbar) findViewById(R.id.Toolbar_UpdateExercice);
        TxtView_NomRecette = (TextView) findViewById(R.id.TxtView_NomRecette);
        ImgView_Recette = (ImageView) findViewById(R.id.ImgView_Image);
        TxtView_Ingredients = (TextView) findViewById(R.id.TxtView_Ingredients);
        TxtView_Preparation = (TextView) findViewById(R.id.TxtView_Preparation);
        BTN_FermerDetail = (Button) findViewById(R.id.BTN_FermerDetail);
        Toolbar_DetailRecette.setTitle("Détail de la recette");
        Toolbar_DetailRecette.setTitleTextColor(Color.rgb(255, 255, 255));
        setSupportActionBar(Toolbar_DetailRecette);

        TxtView_NomRecette.setText(libelle);
        if(!image.equals("")) {
            String uri = "@drawable/" + image;
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            ImgView_Recette.setImageDrawable(res);
        }else {
            String uri = "@drawable/badge_1";
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            ImgView_Recette.setImageDrawable(res);
        }

        TxtView_Ingredients.setText(ingredients);
        TxtView_Preparation.setText(preparation);

        BTN_FermerDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
