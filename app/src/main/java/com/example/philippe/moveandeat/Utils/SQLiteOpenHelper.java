package com.example.philippe.moveandeat.Utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class SQLiteOpenHelper extends android.database.sqlite.SQLiteOpenHelper {

    /**
     * Description générale de la classe :
     *
     *      Cette classe comprend toute la structure de la base de données de l'application.
     *      On y retrouve le nom des tables, des colonnes ainsi que les fonctions permettant la création de la base de données.
     */

    private static final String DATABASE_NAME = "moveandeat.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_USER = "user";
    public static final String COLUMN_USER_ID = "idUser";
    public static final String COLUMN_USER_FIRST_NAME = "prenomU";
    public static final String COLUMN_USER_SEXE = "sexeU";
    public static final String COLUMN_USER_POIDS = "poidsU";
    public static final String COLUMN_USER_TAILLE = "tailleU";
    public static final String COLUMN_USER_OBJECTIF = "objectif";
    public static final String COLUMN_USER_CREATED_AT = "date_created";
    public static final String COLUMN_USER_XP = "xp_user";
    public static final String COLUMN_USER_LEVEL = "lever";

    public static final String TABLE_FAMILLE_EXERCICES = "famille_exercices";
    public static final String COLUMN_FAMILLE_EXERCICES_ID = "famille_exercices_id";
    public static final String COLUMN_FAMILLE_EXERCICES_LIBELLE = "famille_exercices_libelle";

    public static final String TABLE_EXERCICES = "exercices";
    public static final String COLUMN_EXERCICES_ID = "exercices_id";
    public static final String COLUMN_EXERCICES_LIBELLE = "exercices_libelle";
    public static final String COLUMN_EXERCICES_IMAGE = "exercices_image";
    public static final String COLUMN_EXERCICES_DESCRIPTION = "exercices_description";
    public static final String COLUMN_EXERCICES_ID_FAMILLE = "exercices_id_famille";

    public static final String TABLE_SEANCES = "seances";
    public static final String COLUMN_SEANCES_ID = "seances_id";
    public static final String COLUMN_SEANCES_LIBELLE = "seances_libelle";
    public static final String COLUMN_SEANCES_DESCRIPTION = "seances_description";
    public static final String COLUMN_SEANCES_IS_PREDEF = "seances_id_predef";

    public static final String TABLE_SEANCES_EXERCICES = "seances_exercices";
    public static final String COLUMN_SEANCES_EXERCICES_ID = "seances_id";
    public static final String COLUMN_SEANCES_EXERCICES_ID_EXERCICE = "seances_exercices_id_exercice";
    public static final String COLUMN_SEANCES_EXERCICES_ID_SEANCE = "seances_exercices_id_seance";
    public static final String COLUMN_SEANCES_EXERCICES_NB_REPETITIONS = "seances_exercices_nb_repetition";
    public static final String COLUMN_SEANCES_EXERCICES_NB_SERIES = "seances_exercices_nb_serie";
    public static final String COLUMN_SEANCES_EXERCICES_TEMPS_REPOS = "seances_exercices_temps_repos";

    public static final String TABLE_RECETTES = "recettes";
    public static final String COLUMN_RECETTES_ID = "recettes_id";
    public static final String COLUMN_RECETTES_LIBELLE = "recettes_libelle";
    public static final String COLUMN_RECETTES_IMAGE = "recettes_image";
    public static final String COLUMN_RECETTES_INGREDIENTS = "recettes_ingredients";
    public static final String COLUMN_RECETTES_PREPARATION = "recettes_preparation";
    public static final String COLUMN_RECETTES_ID_FAMILLE = "recettes_id_famille";

    public static final String TABLE_FAMILLE_RECETTES = "famille_recettes";
    public static final String COLUMN_FAMILLE_RECETTES_ID = "famille_recettes_id";
    public static final String COLUMN_FAMILLE_RECETTES_LIBELLE = "famille_recettes_libelle";

    /**
     * Permet de créer la table utilisateur
     */
    private static final String DATABASE_CREATE_TABLE_USER = "create table "
            + TABLE_USER
            + "("
            + COLUMN_USER_ID + " integer primary key autoincrement, "
            + COLUMN_USER_FIRST_NAME + " text not null, "
            + COLUMN_USER_SEXE + " text not null, "
            + COLUMN_USER_POIDS + " text not null, "
            + COLUMN_USER_TAILLE + " text not null, "
            + COLUMN_USER_OBJECTIF + " text not null, "
            + COLUMN_USER_CREATED_AT + " datetime default current_timestamp, "
            + COLUMN_USER_XP + " text not null, "
            + COLUMN_USER_LEVEL + " text not null"
            +");";

    /**
     * Permet de créer la table famille exercices
     */
    private static final String DATABASE_CREATE_TABLE_FAMILLE_EXERCICES = "create table "
            + TABLE_FAMILLE_EXERCICES
            + "("
            + COLUMN_FAMILLE_EXERCICES_ID + " integer primary key autoincrement, "
            + COLUMN_FAMILLE_EXERCICES_LIBELLE + " text not null"
            +");";

    /**
     * Permet de créer la table exercices
     */
    private static final String DATABASE_CREATE_TABLE_EXERCICES = "create table "
            + TABLE_EXERCICES
            + "("
            + COLUMN_EXERCICES_ID + " integer primary key autoincrement, "
            + COLUMN_EXERCICES_LIBELLE + " text not null, "
            + COLUMN_EXERCICES_IMAGE + " text not null, "
            + COLUMN_EXERCICES_DESCRIPTION + " text not null, "
            + COLUMN_EXERCICES_ID_FAMILLE + " text not null"
            +");";

    /**
     * Permet de créer la table séance
     */
    private static final String DATABASE_CREATE_TABLE_SEANCES = "create table "
            + TABLE_SEANCES
            + "("
            + COLUMN_SEANCES_ID + " integer primary key autoincrement, "
            + COLUMN_SEANCES_LIBELLE + " text not null, "
            + COLUMN_SEANCES_DESCRIPTION + " text not null, "
            + COLUMN_SEANCES_IS_PREDEF + " integer"
            +");";

    /**
     * Permet de créer la table pivot entre séance et exercice (c'est à dire que cette table contiendra l'id de la
     * séance avec l'id de l'exercice)
     */
    private static final String DATABASE_CREATE_TABLE_SEANCES_EXERCICES = "create table "
            + TABLE_SEANCES_EXERCICES
            + "("
            + COLUMN_SEANCES_EXERCICES_ID + " integer primary key autoincrement, "
            + COLUMN_SEANCES_EXERCICES_ID_EXERCICE + " integer, "
            + COLUMN_SEANCES_EXERCICES_ID_SEANCE + " integer, "
            + COLUMN_SEANCES_EXERCICES_NB_REPETITIONS + " integer, "
            + COLUMN_SEANCES_EXERCICES_NB_SERIES + " integer, "
            + COLUMN_SEANCES_EXERCICES_TEMPS_REPOS + " integer"
            +");";

    /**
     * Permet de créer la table recettes
     */
    private static final String DATABASE_CREATE_TABLE_RECETTES = "create table "
            + TABLE_RECETTES
            + "("
            + COLUMN_RECETTES_ID + " integer primary key autoincrement, "
            + COLUMN_RECETTES_LIBELLE + " text not null, "
            + COLUMN_RECETTES_IMAGE + " text not null, "
            + COLUMN_RECETTES_INGREDIENTS + " text not null, "
            + COLUMN_RECETTES_PREPARATION + " text not null, "
            + COLUMN_RECETTES_ID_FAMILLE + " text not null"
            +");";

    /**
     * Permet de créer la table famille de recettes (matin, midi, soir)
     */
    private static final String DATABASE_CREATE_TABLE_FAMILLE_RECETTES = "create table "
            + TABLE_FAMILLE_RECETTES
            + "("
            + COLUMN_FAMILLE_RECETTES_ID + " integer primary key autoincrement, "
            + COLUMN_FAMILLE_RECETTES_LIBELLE + " text not null"
            +");";

    /**
     * Constructeur de la classe
     * @param context
     */
    public SQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Fonction directement appeler permettant de créer la base de données
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_TABLE_USER);
        db.execSQL(DATABASE_CREATE_TABLE_FAMILLE_EXERCICES);
        db.execSQL(DATABASE_CREATE_TABLE_EXERCICES);
        db.execSQL(DATABASE_CREATE_TABLE_SEANCES);
        db.execSQL(DATABASE_CREATE_TABLE_SEANCES_EXERCICES);
        db.execSQL(DATABASE_CREATE_TABLE_FAMILLE_RECETTES);
        db.execSQL(DATABASE_CREATE_TABLE_RECETTES);
    }

    /**
     * Permet d'upgrader la base de données en fonction de la version qui est en attribut de classe
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAMILLE_EXERCICES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXERCICES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEANCES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEANCES_EXERCICES);
        db.execSQL(DATABASE_CREATE_TABLE_FAMILLE_RECETTES);
        db.execSQL(DATABASE_CREATE_TABLE_RECETTES);
        onCreate(db);
    }
}
