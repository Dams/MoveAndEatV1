package com.example.philippe.moveandeat.Utils;

public class Utils {

    /**
     * Description générale de la classe :
     *
     *      Cette classe contient des fonctions sous formes static (instanciation de la classe non obligatoire).
     *      Ces fonctions sont utiles dans tout le projet comme par exemple la récupération du niveau de l'utilisateur
     *      qui ce fait à plusieurs endroits.
     */

    /**
     * Fonction permettant de revoyer le niveau de l'utilisateur en fonction de son XP
     * @param xp
     */
    public static int getLevelOfUser(int xp){
        int level;
        if(xp < 500){
            level = 1;
        }else if(xp >= 500 && xp < 1000){
            level = 2;
        }else if(xp >= 1000 && xp < 2000){
            level = 3;
        }else if(xp >= 2000 && xp < 3000){
            level = 4;
        }else if(xp >= 3000 && xp < 4000){
            level = 5;
        }else if(xp >= 4000 && xp < 5000){
            level = 6;
        }else if(xp >= 5000 && xp < 10000){
            level = 7;
        }else if(xp >= 10000 && xp < 15000){
            level = 8;
        }else if(xp >= 15000 && xp < 20000){
            level = 9;
        }else if(xp >= 20000 && xp < 25000){
            level = 10;
        }else if(xp >= 25000 && xp < 30000){
            level = 11;
        }else if(xp >= 30000 && xp < 40000){
            level = 12;
        }else if(xp >= 40000 && xp < 50000){
            level = 13;
        }else if(xp >= 50000 && xp < 60000){
            level = 14;
        }else if(xp >= 60000 && xp < 70000){
            level = 15;
        }else if(xp >= 70000 && xp < 80000){
            level = 16;
        }else if(xp >= 80000 && xp < 90000){
            level = 17;
        }else if(xp >= 90000 && xp < 100000){
            level = 18;
        }else if(xp >= 100000 && xp < 200000){
            level = 19;
        }else{
            level = 20;
        }
        return level;
    }

    public static int getNextXpUser(int xp){
        int level;
        if(xp < 500){
            level = 500;
        }else if(xp >= 500 && xp < 1000){
            level = 1000;
        }else if(xp >= 1000 && xp < 2000){
            level = 2000;
        }else if(xp >= 2000 && xp < 3000){
            level = 3000;
        }else if(xp >= 3000 && xp < 4000){
            level = 4000;
        }else if(xp >= 4000 && xp < 5000){
            level = 5000;
        }else if(xp >= 5000 && xp < 10000){
            level = 10000;
        }else if(xp >= 10000 && xp < 15000){
            level = 15000;
        }else if(xp >= 15000 && xp < 20000){
            level = 20000;
        }else if(xp >= 20000 && xp < 25000){
            level = 25000;
        }else if(xp >= 25000 && xp < 30000){
            level = 30000;
        }else if(xp >= 30000 && xp < 40000){
            level = 40000;
        }else if(xp >= 40000 && xp < 50000){
            level = 50000;
        }else if(xp >= 50000 && xp < 60000){
            level = 60000;
        }else if(xp >= 60000 && xp < 70000){
            level = 70000;
        }else if(xp >= 70000 && xp < 80000){
            level = 80000;
        }else if(xp >= 80000 && xp < 90000){
            level = 90000;
        }else if(xp >= 90000 && xp < 100000){
            level = 100000;
        }else if(xp >= 100000 && xp < 200000){
            level = 200000;
        }else{
            level = 200000;
        }
        return level;
    }

    /**
     * Fonction permettant de renvoyer la progression de l'utilisateur qui sera afficher dans la ProgressBar du profil
     * @param xp
     * @return
     */
    public static int getProgressionOfUser(int xp){
        int progression;
        if(xp < 500){
            progression = (100*xp)/500;
        }else if(xp >= 500 && xp < 1000){
            progression = (100*xp)/1000;
        }else if(xp >= 1000 && xp < 2000){
            progression = (100*xp)/2000;
        }else if(xp >= 2000 && xp < 3000){
            progression = (100*xp)/3000;
        }else if(xp >= 3000 && xp < 4000){
            progression = (100*xp)/4000;
        }else if(xp >= 4000 && xp < 5000){
            progression = (100*xp)/5000;
        }else if(xp >= 5000 && xp < 10000){
            progression = (100*xp)/10000;
        }else if(xp >= 10000 && xp < 15000){
            progression = (100*xp)/15000;
        }else if(xp >= 15000 && xp < 20000){
            progression = (100*xp)/20000;
        }else if(xp >= 20000 && xp < 25000){
            progression = (100*xp)/25000;
        }else if(xp >= 25000 && xp < 30000){
            progression = (100*xp)/30000;
        }else if(xp >= 30000 && xp < 40000){
            progression = (100*xp)/40000;
        }else if(xp >= 40000 && xp < 50000){
            progression = (100*xp)/50000;
        }else if(xp >= 50000 && xp < 60000){
            progression = (100*xp)/60000;
        }else if(xp >= 60000 && xp < 70000){
            progression = (100*xp)/70000;
        }else if(xp >= 70000 && xp < 80000){
            progression = (100*xp)/80000;
        }else if(xp >= 80000 && xp < 90000){
            progression = (100*xp)/90000;
        }else if(xp >= 90000 && xp < 100000){
            progression = (100*xp)/100000;
        }else if(xp >= 100000 && xp < 200000){
            progression = (100*xp)/200000;
        }else{
            progression = 100;
        }
        return (100-progression);
    }
}
